package ca.sharcnet.nerve;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

@EnableScheduling
@EnableCaching
@EnableResourceServer
@SpringBootApplication(scanBasePackages = "ca.sharcnet.nerve")
public class NSSIApplication {
    public static void main(String[] args) {
        SpringApplication.run(NSSIApplication.class, args);
    }
}
