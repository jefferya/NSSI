package ca.sharcnet.nerve.broker.message;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * A class that represents a selection within a text.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Selection {
    /**
     * The start character position of the selection.
     */
    private long start;

    /**
     * The end character position of the selection.
     */
    private long end;
}
