package ca.sharcnet.nerve.broker.message;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProgressMessage {
    /**
     * The job ID.
     */
    private Long jobId;

    /**
     * The request ID.
     */
    private String requestId;

    /**
     * The name of the service that sent this progress message.
     */
    private String serviceName;

    /**
     * The number of new tasks started for the given job.
     */
    private Integer numStarted;

    /**
     * The number of new tasks completed for the given job.
     */
    private Integer numCompleted;

    /**
     * The number of new transient failures for the given job.
     */
    private Integer numTransientFailures;

    /**
     * The number of new permanent failures for the given job.
     */
    private Integer numPermanentFailures;

    /**
     * Whether the given job has been completed.
     */
    private Boolean completed;

    /**
     * Whether the given job has failed.
     */
    private Boolean failed;
}
