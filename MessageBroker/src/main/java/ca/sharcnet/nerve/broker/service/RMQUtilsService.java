package ca.sharcnet.nerve.broker.service;

import ca.sharcnet.nerve.broker.MessageBrokerModuleConstants;
import ca.sharcnet.nerve.broker.infrastructure.config.RMQConfiguration;
import ca.sharcnet.nerve.broker.message.ProgressMessage;
import ca.sharcnet.nerve.broker.message.ResultMessage;
import ca.sharcnet.nerve.broker.message.ServiceMessage;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.core.QueueInformation;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

import static net.logstash.logback.argument.StructuredArguments.kv;

@Service
public class RMQUtilsService {
    private final RabbitTemplate rabbitTemplate;
    private final RabbitAdmin rabbitAdmin;
    private final int maxRetries;
    private final ObjectMapper objectMapper;

    private final Logger LOGGER = LoggerFactory.getLogger(RMQUtilsService.class);

    @Autowired
    RMQUtilsService(RabbitTemplate rabbitTemplate,
                    @Value("${nssi.rmq.max-retries:0}") int maxRetries,
                    ObjectMapper objectMapper,
                    RabbitAdmin rabbitAdmin)
    {
        this.rabbitTemplate = rabbitTemplate;
        this.maxRetries = maxRetries;
        this.objectMapper = objectMapper;
        this.rabbitAdmin = rabbitAdmin;
    }

    public <T extends ServiceMessage> void processWithRetry(Message message,
                                                            Class<T> messageType,
                                                            ThrowingConsumer<T, Throwable> messageConsumer)
    {
        processWithRetry(message, messageType, messageConsumer, (t) -> true);
    }

    public <T extends ServiceMessage> void processWithRetry(Message message,
                                                            Class<T> messageType,
                                                            ThrowingConsumer<T, Throwable> messageConsumer,
                                                            Predicate<Throwable> isRetryable)
    {
        T serviceMessage = null;
        try {
            serviceMessage = objectMapper.readValue(message.getBody(), messageType);
            messageConsumer.accept(serviceMessage);
        } catch (Throwable t) {
            LOGGER.error("FAILED To Process Message",  
                        kv("stackTrace", t.getStackTrace()), 
                        kv("service", MessageBrokerModuleConstants.SERVICE_NAME),
                        kv("jobId", getJobIdFromMessage(message, messageType).orElse(-1L)),
                        kv("failureLocation", message.getMessageProperties().getConsumerQueue()),
                        kv("requestId", getRequestIdFromMessage(message, messageType).orElse("-1")));

            MessageProperties props = message.getMessageProperties();
            int retriedCount = Optional
                    .ofNullable(props.<Integer>getHeader("x-retried-count"))
                    .orElse(0);

            if (isRetryable.test(t) && retriedCount < maxRetries) {
                String targetRetryQueue = RMQConfiguration.RETRY_QUEUES[retriedCount];
                props.setHeader("x-retried-count", retriedCount + 1);
                props.setHeader("x-original-exchange", props.getReceivedExchange());
                props.setHeader("x-original-routing-key", props.getReceivedRoutingKey());

                LOGGER.info("Retrying Failed Message",
                           kv("service", MessageBrokerModuleConstants.SERVICE_NAME),
                           kv("jobId", getJobIdFromMessage(message, messageType).orElse(-1L)),
                           kv("targetQueue", targetRetryQueue),
                           kv("requestId", getRequestIdFromMessage(message, messageType).orElse("-1")));
                rabbitTemplate.convertAndSend(targetRetryQueue, message);

                Optional.ofNullable(serviceMessage).ifPresent(m ->
                        sendProgressMessage(ProgressMessage.builder()
                                .jobId(m.getJobId())
                                .requestId(m.getRequestId())
                                .serviceName(RMQUtilsService.class.getName())
                                .numTransientFailures(1)
                                .build()));
            } else {
                LOGGER.info("Sending Failed Message To Parking Lot",
                           kv("service", MessageBrokerModuleConstants.SERVICE_NAME),
                           kv("jobId", getJobIdFromMessage(message, messageType).orElse(-1L)),
                           kv("requestId", getRequestIdFromMessage(message, messageType).orElse("-1")));
                rabbitTemplate.convertAndSend(RMQConfiguration.PARKING_LOT_QUEUE, message);

                Optional.ofNullable(serviceMessage).ifPresent(m ->
                        sendProgressMessage(ProgressMessage.builder()
                                .jobId(m.getJobId())
                                .requestId(m.getRequestId())
                                .serviceName(RMQUtilsService.class.getName())
                                .numPermanentFailures(1)
                                .build()));
            }
        }
    }

    public void sendToNextStep(ServiceMessage message) {
        List<String> pipeline = message.getPipeline();
        String next = pipeline.get(0);
        List<String> remainingAfterNext = pipeline.subList(1, pipeline.size());
        message.setPipeline(remainingAfterNext);

        rabbitTemplate.convertAndSend(RMQConfiguration.APP_MESSAGES_EXCHANGE, next, message);
    }

    public void sendProgressMessage(ProgressMessage message) {
        rabbitTemplate.convertAndSend(RMQConfiguration.PROGRESS_EXCHANGE, RMQConfiguration.PROGRESS_QUEUE, message);
    }

    public void sendResultMessage(ResultMessage message) {
        rabbitTemplate.convertAndSend(RMQConfiguration.PROGRESS_EXCHANGE, RMQConfiguration.RESULT_QUEUE, message);
    }

    public Integer numInQueue(String queueName) {
        return Optional.ofNullable(rabbitAdmin.getQueueInfo(queueName))
                .map(QueueInformation::getMessageCount)
                .orElse(0);
    }

    public <T extends ServiceMessage> Optional<Long> getJobIdFromMessage(Message message, Class<T> messageType) {
        try {
            T serviceMessage = objectMapper.readValue(message.getBody(), messageType);
            return Optional.ofNullable(serviceMessage.getJobId());
        } catch (Exception e){
            LOGGER.error("FAILED To Get Job Id",  
                        kv("stackTrace", e.getStackTrace()), 
                        kv("service", MessageBrokerModuleConstants.SERVICE_NAME));
            return Optional.empty();
        }
    }

    public <T extends ServiceMessage> Optional<String> getRequestIdFromMessage(Message message, Class<T> messageType) {
        try {
            T serviceMessage = objectMapper.readValue(message.getBody(), messageType);
            return Optional.ofNullable(serviceMessage.getRequestId());
        } catch (Exception e){
            LOGGER.error("FAILED To Get Request Id",  
                        kv("stackTrace", e.getStackTrace()), 
                        kv("service", MessageBrokerModuleConstants.SERVICE_NAME));
            return Optional.empty();
        }
    }
}
