package ca.sharcnet.nerve.broker.message;

public enum EntityClassification {
    PERSON,
    ORGANIZATION,
    LOCATION,
    TITLE, // is this correct? is it person titles, like "king"?
    MISC
}
