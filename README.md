# NSSI

**NERVE Secure Scalable Infrastructure**

NSSI is a platform for running data conversion workflows for generating linked data from structured and semi-structured data. Clients submit jobs
to NSSI specifying a predefined workflow, and receive a URI from where they can retrieve results when the processing completes.

## Building and running NSSI

These instructions cover how to build and run the application locally from source.

Prerequisites:

 * Oracle JDK 13
 * Maven
 * Docker Compose

### Importing the project

To set up the project in IntelliJ or your IDE of choice, import the pom.xml under the repository root (not the pom files under any of the subdirectories).

### Building the project

#### Checkout the repository

```bash
git clone git@gitlab.com:calincs/conversion/NSSI.git
or
git clone https://gitlab.com/calincs/conversion/NSSI.git
```

#### Build a local Docker image

There is a Maven wrapper included in the project which can be used to invoke build and test actions. To build a local Docker image, run the following from the repository root:

```bash
./mvnw clean install && ./mvnw jib:dockerBuild -pl Service/ -Ddebug
```

You should see a newly created NSSI Docker image:

```bash
$ docker images
REPOSITORY                                                TAG                 IMAGE ID            CREATED             SIZE
registry.gitlab.com/calincs/conversion/nssi/v2            latest-dev          38e8b398425e        10 seconds ago      340MB
```

### Running with Docker Compose

There are several Docker Compose files available in the repository to run NSSI in different configurations.
See the NSSI wiki for more details: https://gitlab.com/calincs/conversion/NSSI/-/wikis/home.
