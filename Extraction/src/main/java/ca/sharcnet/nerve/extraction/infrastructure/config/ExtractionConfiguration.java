package ca.sharcnet.nerve.extraction.infrastructure.config;

import ca.sharcnet.nerve.extraction.ExtractionModuleConstants;
import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import static ca.sharcnet.nerve.broker.infrastructure.config.RMQConfiguration.*;
import static ca.sharcnet.nerve.extraction.ExtractionModuleConstants.QUEUE_NAME;

@Profile(ExtractionModuleConstants.PROFILE_NAME)
@Configuration
public class ExtractionConfiguration {

    @Bean
    public Queue extractionQueue() {
        return QueueBuilder.durable(QUEUE_NAME)
                .withArgument(DLQ_KEY, DLX_APP_MESSAGES)
                .build();
    }

    @Bean
    public Binding extractionBinding(@Qualifier(APP_MESSAGES_EXCHANGE) DirectExchange exchange) {
        return BindingBuilder.bind(extractionQueue()).to(exchange)
                .with(QUEUE_NAME);
    }
}
