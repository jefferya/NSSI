package ca.sharcnet.nerve.extraction.service;

import ca.sharcnet.nerve.broker.message.NSSIServiceMessage;
import ca.sharcnet.nerve.broker.service.RMQUtilsService;
import ca.sharcnet.nerve.extraction.ExtractionModuleConstants;
import ca.sharcnet.nerve.job.service.WorkflowETAService;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Optional;

import static net.logstash.logback.argument.StructuredArguments.kv;

@Profile(ExtractionModuleConstants.PROFILE_NAME)
@Service
public class ExtractionService {
    private RMQUtilsService rmqUtils;
    private RestTemplate restTemplate;
    private WorkflowETAService etaService;
    private int numMessages = 0;

    private final Logger LOGGER = LoggerFactory.getLogger(ExtractionService.class);

    @Autowired
    public ExtractionService(RMQUtilsService rmqUtils,
                             RestTemplate restTemplate,
                             WorkflowETAService etaService) {
        this.rmqUtils = rmqUtils;
        this.restTemplate = restTemplate;
        this.etaService = etaService;
    }

    @RabbitListener(queues = ExtractionModuleConstants.QUEUE_NAME)
    public void listen(Message message) {
        numMessages = etaService.checkUpdateAverage(rmqUtils.numInQueue(ExtractionModuleConstants.QUEUE_NAME), 
                                                    numMessages, 
                                                    ExtractionModuleConstants.QUEUE_NAME);
        rmqUtils.processWithRetry(message, NSSIServiceMessage.class, (startMessage -> {
            LOGGER.info("Processing Extraction Message",  
                       kv("service", ExtractionModuleConstants.SERVICE_NAME),
                       kv("jobId", startMessage.getJobId()),
                       kv("documentUri", startMessage.getDocumentURI()),
                       kv("requestId", startMessage.getRequestId()));

            NSSIServiceMessage resultMessage = documentContents(startMessage)
                    .map(contents -> startMessage.toBuilder()
                            .document(contents)
                            .build()
                    ).orElseThrow();

            rmqUtils.sendToNextStep(resultMessage);
        }));
    }

    private Optional<String> documentContents(NSSIServiceMessage message) {
        String documentURI = message.getDocumentURI();

        switch (message.getFormat()) {
            case TEI_XML:
                return fetchTEIXML(documentURI);
            case ORLANDO_XML:
                return fetchOrlandoXML(documentURI);
            case MODS:
                return fetchMODS(documentURI);
            case EAD:
                return fetchEAD(documentURI);
            case PDF:
                return fetchPDF(documentURI);
            case HTML:
                return fetchHTML(documentURI);
            case PLAIN_TEXT:
                return fetchText(documentURI);
            default:
                return Optional.empty();
        }
    }

    private Optional<String> fetchTEIXML(String documentURI) {
        String result = restTemplate.getForObject(documentURI, String.class);

        return Optional.ofNullable(result);
    }

    private Optional<String> fetchOrlandoXML(String documentURI) {
        String result = restTemplate.getForObject(documentURI, String.class);

        return Optional.ofNullable(result);
    }

    private Optional<String> fetchMODS(String documentURI) {
        // TODO
        String result = restTemplate.getForObject(documentURI, String.class);

        return Optional.ofNullable(result);
    }

    private Optional<String> fetchEAD(String documentURI) {
        // TODO
        String result = restTemplate.getForObject(documentURI, String.class);

        return Optional.ofNullable(result);
    }

    private Optional<String> fetchPDF(String documentURI) {
        // TODO
        return Optional.empty();
    }

    private Optional<String> fetchHTML(String documentURI) {
        try {
            Document document = Jsoup.connect(documentURI).get();
            Element body = document.select("body").first(); // TODO??

            return Optional.ofNullable(body).map(Element::text);
        } catch (HttpStatusException e) {
            // TODO
            return Optional.empty();
        } catch (IOException e) {
            // TODO
            return Optional.empty();
        }
    }

    private Optional<String> fetchText(String documentURI) {
        String result = restTemplate.getForObject(documentURI, String.class);

        return Optional.ofNullable(result);
    }
}
