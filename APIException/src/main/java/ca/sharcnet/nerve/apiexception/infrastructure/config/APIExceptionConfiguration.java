package ca.sharcnet.nerve.apiexception.infrastructure.config;

import ca.sharcnet.nerve.apiexception.APIExceptionModuleConstants;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Profile(APIExceptionModuleConstants.PROFILE_NAME)
@Configuration
public class APIExceptionConfiguration {

}
