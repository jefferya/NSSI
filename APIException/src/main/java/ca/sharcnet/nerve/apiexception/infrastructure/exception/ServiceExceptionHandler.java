package ca.sharcnet.nerve.apiexception.infrastructure.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ServiceExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(DeleteIncompleteJobException.class)
    public final ResponseEntity handleDeleteException(DeleteIncompleteJobException e) {
        var error = CustomExceptionSchema.builder()
                .message(String.format("Job ID %d could not be deleted.", e.getJobId()))
                .details(String.format("The specified job may still be in progress. To send a " +
                        "cancel request instead, use `PUT /api/jobs/%d/actions/cancel`", e.getJobId()))
                .build();
        return new ResponseEntity(error, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(JobNotReadyException.class)
    public final ResponseEntity handlePrematureResultsException(JobNotReadyException e) {
        var error = CustomExceptionSchema.builder()
                .message(String.format("Job ID %d has not completed processing, so no results " +
                        "are yet available.", e.getJobId()))
                .build();
        return new ResponseEntity(error, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(JobCancelledOrFailedException.class)
    public final ResponseEntity handleJobCancelledOrFailedException(JobCancelledOrFailedException e) {
        var error = CustomExceptionSchema.builder()
                .message(String.format("Job ID %d has cancelled or failed status.", e.getJobId()))
                .build();
        return new ResponseEntity(error, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(CancelCompletedJobException.class)
    public final ResponseEntity handleCancelException (CancelCompletedJobException e) {
        var error = CustomExceptionSchema.builder()
                .message(String.format("Job ID %d could not be cancelled.", e.getJobId()))
                .details(String.format("The specified job has already completed. To send a " +
                        "delete request instead, use `DELETE /api/jobs/%d`", e.getJobId()))
                .build();
        return new ResponseEntity(error, HttpStatus.CONFLICT);
    }
}
