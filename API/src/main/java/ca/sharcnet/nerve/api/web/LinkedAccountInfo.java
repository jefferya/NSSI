package ca.sharcnet.nerve.api.web;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LinkedAccountInfo {
    /**
     * A string identifying the external identity provider for the linked account (e.g. "github").
     */
    private String identityProvider;

    /**
     * The user ID of the linked account.
     */
    private String userId;

    /**
     * The username of the linked account.
     */
    private String userName;
}
