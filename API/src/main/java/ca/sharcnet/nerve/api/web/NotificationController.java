package ca.sharcnet.nerve.api.web;

import ca.sharcnet.nerve.api.APIModuleConstants;
import ca.sharcnet.nerve.notification.service.NotificationService;
import org.keycloak.TokenVerifier;
import org.keycloak.common.VerificationException;
import org.keycloak.representations.AccessToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import static net.logstash.logback.argument.StructuredArguments.kv;

/**
 * This controller provides an endpoint through which front-end applications
 * can register to receive notifications on job updates. The notifications
 * mechanism is detailed here:
 *  https://gitlab.com/calincs/conversion/NSSI/-/wikis/Usage/Client-Notifications.
 */

@Profile(APIModuleConstants.PROFILE_NAME)
@Transactional
@RestController
public class NotificationController {
    private final Logger LOGGER = LoggerFactory.getLogger(NotificationController.class);
    private final NotificationService notifyService;
    private final Long emitterTimeout;

    @Autowired
    public NotificationController(NotificationService notifyService,
                                  @Value("${nssi.api.notifications.timeout_period_in_ms}") Long emitterTimeout) {
        this.notifyService = notifyService;
        this.emitterTimeout = emitterTimeout;
    }

    /**
     * Registers an event emitter for the job whose details are provided in the token cookie.
     *
     * @param kcToken the cookie containing the Keycloak auth token
     * @param requestId the request ID, assigned by the NSSI Gateway
     */
    @GetMapping(value = "/api/notifications")
    public ResponseEntity<SseEmitter> accessClient(@CookieValue(value="kcToken") String kcToken,
                                                   @RequestHeader("requestId") String requestId) {
        try {
            AccessToken token = TokenVerifier.create(kcToken, AccessToken.class).getToken();   
            String clientId = token.getIssuedFor();

            SseEmitter emitter = new SseEmitter(emitterTimeout);
            notifyService.addClient(clientId, emitter, requestId);
            return new ResponseEntity<>(emitter, HttpStatus.OK);
        } catch (VerificationException e) {
            LOGGER.error("FAILED Invalid Token For Notification",  
                        kv("stackTrace", e.getStackTrace()), 
                        kv("service", APIModuleConstants.SERVICE_NAME),
                        kv("requestId", requestId));
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
