package ca.sharcnet.nerve.api.web;

import ca.sharcnet.nerve.api.APIModuleConstants;
import ca.sharcnet.nerve.api.infrastructure.config.NSSIProcessingConfigProperties;
import ca.sharcnet.nerve.api.infrastructure.config.WorkflowResultsType;
import ca.sharcnet.nerve.api.service.UploadsService;
import ca.sharcnet.nerve.apiexception.infrastructure.exception.CancelCompletedJobException;
import ca.sharcnet.nerve.apiexception.infrastructure.exception.DeleteIncompleteJobException;
import ca.sharcnet.nerve.apiexception.infrastructure.exception.ResourceNotFoundException;
import ca.sharcnet.nerve.broker.message.CancelMessage;
import ca.sharcnet.nerve.broker.message.NSSIServiceMessage;
import ca.sharcnet.nerve.broker.service.RMQUtilsService;
import ca.sharcnet.nerve.elucidate_api.service.AnnotationClient;
import ca.sharcnet.nerve.job.model.JobDTO;
import ca.sharcnet.nerve.job.model.JobStatus;
import ca.sharcnet.nerve.job.service.JobService;
import ca.sharcnet.nerve.job.service.WorkflowETAService;
import org.keycloak.TokenVerifier;
import org.keycloak.common.VerificationException;
import org.keycloak.representations.AccessToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.security.Principal;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import static net.logstash.logback.argument.StructuredArguments.kv;

/**
 * This controller provides the job-related endpoints for the NSSI API.
 */

@Profile(APIModuleConstants.PROFILE_NAME)
@RestController
public class JobsController {
    private final JobService jobService;
    private final UploadsService uploadsService;
    private final AnnotationClient annotationClient;
    private final RMQUtilsService rmqUtils;
    private final NSSIProcessingConfigProperties processingConfigProperties;
    private final WorkflowETAService workflowETAService;
    private final String baseUrl;

    private final Logger LOGGER = LoggerFactory.getLogger(JobsController.class);

    @Autowired
    JobsController(JobService jobService,
                   UploadsService uploadsService,
                   AnnotationClient annotationClient,
                   RMQUtilsService rmqUtils,
                   @Value("${application.base_url_external}") String baseUrl,
                   NSSIProcessingConfigProperties processingConfigProperties,
                   WorkflowETAService workflowETAService)
    {
        this.jobService = jobService;
        this.uploadsService = uploadsService;
        this.annotationClient = annotationClient;
        this.rmqUtils = rmqUtils;
        this.processingConfigProperties = processingConfigProperties;
        this.workflowETAService = workflowETAService;
        this.baseUrl = baseUrl;
    }

    /**
     * Starts a new processing job, with the data to process attached to
     * the request as a file.
     *
     * This will upload the data to the NSSI fileserver (as if a request to the
     * upload API had been made with that data), and will then proceed with the
     * processing pipeline using the URI of this uploaded file as the document URI.
     *
     * @param principal automatically set by Spring, this object represents the authenticated user
     * @param request the submission request (see {@link ca.sharcnet.nerve.api.web.SubmissionRequest})
     * @param file the data to process
     * @param authToken the auth token for the request
     * @param requestId the request ID, assigned by the NSSI Gateway
     */
    @PostMapping(value = "/api/jobs", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<JobInfo> submitFileUpload(Principal principal,
                                                    @RequestPart("request") SubmissionRequest request,
                                                    @RequestPart("file") MultipartFile file,
                                                    @RequestHeader("Authorization") String authToken,
                                                    @RequestHeader("requestId") String requestId) throws VerificationException
    {
        try {
            if (file.getBytes().length == 0
                    || (Objects.nonNull(file.getOriginalFilename()) && file.getOriginalFilename().isBlank())) {

                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }

            String uploadedDocumentURI = uploadsService.save(file, principal.getName(), requestId);
            request.setDocumentURI(uploadedDocumentURI);
        } catch (IOException e) {
            LOGGER.error("FAILED To Submit FIle Upload",  
                        kv("stackTrace", e.getStackTrace()), 
                        kv("service", APIModuleConstants.SERVICE_NAME),
                        kv("fileName", file.getOriginalFilename()),
                        kv("requestId", requestId));
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return submit(principal, request, authToken, requestId);
    }

    /**
     * Starts a new processing job.
     *
     * @param principal automatically set by Spring, this object represents the authenticated user
     * @param request the submission request (see {@link ca.sharcnet.nerve.api.web.SubmissionRequest})
     * @param authToken the auth token for the request
     * @param requestId the request ID, assigned by the NSSI Gateway
     */
    @PostMapping(value = "/api/jobs", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<JobInfo> submit(Principal principal,
                                          @RequestBody SubmissionRequest request,
                                          @RequestHeader("Authorization") String authToken,
                                          @RequestHeader("requestId") String requestId) throws VerificationException 
    {
        String[] cleanAuthToken = authToken.split("\\s+");
        AccessToken token = TokenVerifier.create(cleanAuthToken[1], AccessToken.class).getToken();   
        String clientId = token.getIssuedFor();

        String workflow = request.getWorkflow();
        Long jobId = jobService.newJob(principal.getName(), request.getContext(), workflow, clientId, requestId);
        String resultsUri = start(request, jobId, requestId);

        jobService.updateResultsUri(jobId, resultsUri);

        return new ResponseEntity<>(
                JobInfo.builder()
                        .jobId(jobId)
                        .resultsUri(resultsUri)
                        .status(JobStatus.IN_PROGRESS)
                        .estimatedMinUntilRun(workflowETAService.getWorkflowETA(workflow))
                        .build(),
                HttpStatus.ACCEPTED
        );
    }

    /**
     * Gets the info for the specified job.
     *
     * @param principal automatically set by Spring, this object represents the authenticated user
     * @param jobId the job ID
     * @param authToken the auth token for the request
     * @param requestId the request ID, assigned by the NSSI Gateway
     */
    @GetMapping(value = "/api/jobs/{jobId}")
    public ResponseEntity<JobInfo> info(Principal principal,
                                        @PathVariable("jobId") Long jobId,
                                        @RequestHeader("Authorization") String authToken,
                                        @RequestHeader("requestId") String requestId)
            throws ResourceNotFoundException
    {
        JobDTO job = jobService.getJob(jobId).orElseThrow(ResourceNotFoundException::new);

        return new ResponseEntity<>(
                JobInfo.builder()
                        .jobId(jobId)
                        .resultsUri(job.getResultsUri())
                        .status(job.getStatus())
                        .build(),
                HttpStatus.OK
        );
    }

    /**
     * Cancels the specified job, if it has not already completed.
     *
     * @param principal automatically set by Spring, this object represents the authenticated user
     * @param jobId the job ID
     * @param authToken the auth token for the request
     * @param requestId the request ID, assigned by the NSSI Gateway
     */
    @PutMapping(value = "/api/jobs/{jobId}/actions/cancel")
    public ResponseEntity cancel(Principal principal,
                                 @PathVariable("jobId") Long jobId,
                                 @RequestHeader("Authorization") String authToken,
                                 @RequestHeader("requestId") String requestId)
            throws CancelCompletedJobException, ResourceNotFoundException
    {
        JobDTO job = jobService.getJob(jobId).orElseThrow(ResourceNotFoundException::new);

        if (!job.getStatus().equals(JobStatus.IN_PROGRESS)) {
            throw new CancelCompletedJobException(jobId);
        }

        String workflow = job.getWorkflow();
        Optional.ofNullable(processingConfigProperties.getServiceConfigs(workflow))
                .ifPresent(pipeline -> pipeline.forEach(serviceConfig ->
                        Optional.ofNullable(serviceConfig.getCleanupQueue())
                                .ifPresent(queue -> cancel(jobId, queue))
                ));

        jobService.cancelJob(jobId);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    /**
     * Deletes the specified job, if it is not still in progress.
     *
     * @param principal automatically set by Spring, this object represents the authenticated user
     * @param jobId the job ID
     * @param authToken the auth token for the request
     * @param requestId the request ID, assigned by the NSSI Gateway
     */
    @DeleteMapping(value = "/api/jobs/{jobId}")
    public ResponseEntity delete(Principal principal,
                                 @PathVariable("jobId") Long jobId,
                                 @RequestHeader("Authorization") String authToken,
                                 @RequestHeader("requestId") String requestId)
            throws DeleteIncompleteJobException, ResourceNotFoundException
    {
        LOGGER.info("Deleting Job",  
                   kv("service", APIModuleConstants.SERVICE_NAME),
                   kv("jobId", jobId),
                   kv("user", principal.getName()),
                   kv("requestId", requestId));

        JobDTO job = jobService.getJob(jobId).orElseThrow(ResourceNotFoundException::new);

        if (job.getStatus().equals(JobStatus.IN_PROGRESS)) {
            throw new DeleteIncompleteJobException(jobId);
        }

        jobService.deleteJob(jobId);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    /**
     * Constructs and sends a start message to the appropriate queue in RMQ,
     * based on the workflow specified in the request.
     *
     * @param request the submission request (see {@link ca.sharcnet.nerve.api.web.SubmissionRequest})
     * @param jobId the job ID
     * @param requestId the request ID, assigned by the NSSI Gateway
     */
    private String start(SubmissionRequest request, Long jobId, String requestId /* FIXME, MDC */) {
        String workflow = request.getWorkflow();
        String resultsUri = getResultsUri(workflow, request.getDocumentURI(), jobId, requestId);

        List<String> pipeline =
                Optional.ofNullable(processingConfigProperties.getServiceConfigs(workflow))
                        .orElse(Collections.emptyList()).stream()
                        .map(NSSIProcessingConfigProperties.ServiceConfig::getJobQueue)
                        .collect(Collectors.toList());

        NSSIServiceMessage message = NSSIServiceMessage.builder()
                .jobId(jobId)
                .projectName(request.getProjectName())
                .pipeline(pipeline)
                .documentURI(request.getDocumentURI())
                .document(request.getDocument())
                .format(request.getFormat())
                .authorities(request.getAuthorities())
                .context(request.getContext())
                .resultsUri(resultsUri)
                .requestId(requestId)
                .build();

        rmqUtils.sendToNextStep(message);

        return resultsUri;
    }

    /**
     * Sends a cancel message to the given {@code queue} for the specified job.
     *
     * @param jobId the job ID
     * @param queue the queue to send a cancel message to
     */
    private void cancel(Long jobId, String queue) {
        var cancelMessage = CancelMessage.builder()
                .jobId(jobId)
                .pipeline(List.of(queue))
                .build();

        rmqUtils.sendToNextStep(cancelMessage);
    }

    /**
     * Returns the URI where results can be found for the given job.
     *
     * @param workflow the workflow of the job
     * @param documentURI the document URI that was processed by the job
     * @param jobId the job ID
     * @param requestId the request ID, assigned by the NSSI Gateway
     */
    private String getResultsUri(String workflow, String documentURI, Long jobId, String requestId) {
        Optional<NSSIProcessingConfigProperties.WorkflowConfig> workflowConfig =
                Optional.ofNullable(processingConfigProperties.getWorkflows().get(workflow));

        return workflowConfig.map(config -> {
            if (config.getResultsType().equals(WorkflowResultsType.ELUCIDATE)) {
                return annotationClient.createNewContainer(documentURI, requestId);
            }

            return baseUrl + "/results/" + workflow + "/" + jobId.toString();
        }).orElse(null);
    }
}
