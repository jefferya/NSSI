package ca.sharcnet.nerve.api.web;

import ca.sharcnet.nerve.api.APIModuleConstants;
import org.keycloak.TokenVerifier;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.common.VerificationException;
import org.keycloak.common.util.Base64Url;
import org.keycloak.common.util.KeycloakUriBuilder;
import org.keycloak.representations.AccessToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * This controller provides endpoints for obtaining user information that is
 * useful for front-end applications.
 */

@Profile(APIModuleConstants.PROFILE_NAME)
@RestController
public class UserInfoController {
    private final Keycloak keycloakAdminClient;
    private final String keycloakRealm;
    private final String keycloakAuthServerUrl;

    private final Logger LOGGER = LoggerFactory.getLogger(UserInfoController.class);

    @Autowired
    UserInfoController(Keycloak keycloakAdminClient,
                       @Value("${keycloak.realm}") String keycloakRealm,
                       @Value("${keycloak.auth-server-url}") String keycloakAuthServerUrl)
    {
        this.keycloakAdminClient = keycloakAdminClient;
        this.keycloakRealm = keycloakRealm;
        this.keycloakAuthServerUrl = keycloakAuthServerUrl;
    }

    /**
     * Returns a list of linked accounts (if any) for the user identified by
     * the provided {@code authToken}.
     *
     * @param principal automatically set by Spring, this object represents the authenticated user
     * @param authToken the auth token for the request
     * @param requestId the request ID, assigned by the NSSI Gateway
     */
    @GetMapping(value = "/api/userinfo/linkedAccounts")
    public List<LinkedAccountInfo> getLinkedAccounts(Principal principal,
                                                     @RequestHeader("Authorization") String authToken,
                                                     @RequestHeader("requestId") String requestId)
    {
        return keycloakAdminClient.realm(keycloakRealm).users()
                .search(principal.getName()).stream().findFirst()
                .map(userRep -> keycloakAdminClient.realm(keycloakRealm).users().get(userRep.getId()))
                .map(UserResource::getFederatedIdentity).orElse(Collections.emptyList()).stream()
                .map(federatedIdentityRepresentation -> LinkedAccountInfo.builder()
                        .userName(federatedIdentityRepresentation.getUserName())
                        .userId(federatedIdentityRepresentation.getUserId())
                        .identityProvider(federatedIdentityRepresentation.getIdentityProvider())
                        .build())
                .collect(Collectors.toList());
    }

    /**
     * Returns a URL to allow a user to link their account to an external ID provider.
     *
     * The front-end application must redirect the browser to the URL returned by this
     * method. That URL will kick-off a Keycloak workflow where users can authenticate
     * with an external ID provider and link their account to that provides. Once the
     * account linking process has completed, Keycloak will redirect the user to the
     * given {@code appRedirectUri}.
     *
     * See here for more details on the client-initiated account linking process in Keycloak:
     * https://wjw465150.gitbooks.io/keycloak-documentation/content/server_development/topics/identity-brokering/account-linking.html
     *
     * @param principal automatically set by Spring, this object represents the authenticated user
     * @param appRedirectUri the final URI to redirect to after account linking is complete
     * @param authToken the auth token for the request
     * @param requestId the request ID, assigned by the NSSI Gateway
     */
    @GetMapping(value = "/api/userinfo/accountLinkUrl")
    public ResponseEntity<String> getRedirectUri(Principal principal,
                                                 @RequestParam("provider") String provider,
                                                 @RequestParam("redirectUri") String appRedirectUri,
                                                 @RequestHeader("Authorization") String authToken,
                                                 @RequestHeader("requestId") String requestId) throws VerificationException
    {
        AccessToken token = TokenVerifier.create(authToken.substring(7), AccessToken.class).getToken();
        String clientId = token.getIssuedFor();
        String nonce = UUID.randomUUID().toString();
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        String input = nonce + token.getSessionState() + clientId + provider;
        byte[] check = md.digest(input.getBytes(StandardCharsets.UTF_8));
        String hash = Base64Url.encode(check);
        String accountLinkUrl = KeycloakUriBuilder.fromUri(keycloakAuthServerUrl)
                .path("/realms/{realm}/broker/{provider}/link")
                .queryParam("nonce", nonce)
                .queryParam("hash", hash)
                .queryParam("client_id", clientId)
                .queryParam("redirect_uri", appRedirectUri)
                .build(keycloakRealm, provider)
                .toString();

        return ResponseEntity.ok(accountLinkUrl);
    }
}
