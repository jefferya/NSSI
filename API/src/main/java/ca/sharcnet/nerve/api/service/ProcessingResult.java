package ca.sharcnet.nerve.api.service;

import java.time.LocalDateTime;
import java.util.Map;

public interface ProcessingResult {
    LocalDateTime getProcessingDate();
    Map<String, String> getMetadata();
    void setProcessingDate(LocalDateTime date);
    void setMetadata(Map<String, String> metadata);
}
