package ca.sharcnet.nerve.api.web;

import ca.sharcnet.nerve.broker.message.DocumentMIMEType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SubmissionRequest {
    /**
     * The name of the project under which this job is submitted.
     */
    private String projectName;

    /**
     * The workflow to use for processing this job.
     */
    private String workflow;

    /**
     * A URI for the document to process. If the {@link SubmissionRequest#document}
     * field is null, the document contents will be fetched from this URI.
     */
    private String documentURI;

    /**
     * The document contents to process.
     */
    private String document;

    /**
     * The MIME type of the document.
     */
    private DocumentMIMEType format;

    /**
     * A list of authorities to use for reconciliation.
     */
    private List<String> authorities;

    /**
     * A map containing any other service-specific context that might be needed
     * to process the submission.
     */
    private Map<String, String> context;
}
