package ca.sharcnet.nerve.api.service;

import ca.sharcnet.nerve.api.APIModuleConstants;
import ca.sharcnet.nerve.job.model.JobDTO;
import ca.sharcnet.nerve.s3_api.service.LincsS3Service;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static net.logstash.logback.argument.StructuredArguments.kv;

@Profile(APIModuleConstants.PROFILE_NAME)
@Service
public class S3ResultsService {
    private final LincsS3Service s3Service;
    private final ObjectMapper objectMapper;

    private final String BUCKET_NAME_KEY = "S3_BUCKET_NAME";
    private final String OBJECT_NAME_KEY = "S3_OBJECT_NAME";
    private final String OBJECT_PREFIX_KEY = "S3_OBJECT_PREFIX";
    private final String METADATA_OBJECT_NAME_KEY = "S3_METADATA_OBJECT";
    private final String PLAIN_TEXT_KEY = "S3_PLAIN_TEXT";

    private final TypeReference<HashMap<String, String>> STR_MAP_TYPEREF = new TypeReference<>() {
    };
    private final TypeReference<HashMap<String, Object>> OBJ_MAP_TYPEREF = new TypeReference<>() {
    };

    private final Logger LOGGER = LoggerFactory.getLogger(S3ResultsService.class);

    @Autowired
    S3ResultsService(LincsS3Service s3Service,
                     ObjectMapper objectMapper)
    {
        this.s3Service = s3Service;
        this.objectMapper = objectMapper;
    }

    public S3Result getResults(JobDTO job, String requestId) {
        Map<String, String> params = job.getResultStoreParams();
        boolean plainText = Boolean.parseBoolean(params.get(PLAIN_TEXT_KEY));
        
        if (plainText) {
            return S3TextResult.builder()
                    .processingDate(job.getLastUpdatedTime())
                    .metadata(getS3Metadata(job, requestId))
                    .data(getTextData(job, requestId))
                    .build();
        }

        return S3JsonResult.builder()
                .processingDate(job.getLastUpdatedTime())
                .metadata(getS3Metadata(job, requestId))
                .data(getJsonData(job, requestId))
                .build();

    }

    public void deleteResults(JobDTO job, String requestId)
    {
        Map<String, String> params = job.getResultStoreParams();
        String bucket = params.get(BUCKET_NAME_KEY);
        String object = params.get(OBJECT_NAME_KEY);
        String objectPrefix = params.get(OBJECT_PREFIX_KEY);

        if (StringUtils.isNotEmpty(bucket)) {
            if (StringUtils.isNotEmpty(object)) {
                s3Service.deleteObject(bucket, object, requestId);
            } else if (StringUtils.isNotEmpty(objectPrefix)) {
                s3Service.deleteAllObjects(bucket, objectPrefix, requestId);
            }
        }
    }

    private Map<String, String> getS3Metadata(JobDTO job, String requestId) {
        try {
            Map<String, String> params = job.getResultStoreParams();
            String bucket = params.get(BUCKET_NAME_KEY);
            String metadataObject = params.get(METADATA_OBJECT_NAME_KEY);

            if (StringUtils.isNotEmpty(bucket)) {
                if (StringUtils.isNotEmpty(metadataObject)) {
                    byte[] metadataContents = s3Service.getObject(bucket, metadataObject, requestId);
                    return objectMapper.readValue(metadataContents, STR_MAP_TYPEREF);
                }
            }

        } catch (IOException e) {
            LOGGER.error("FAILED To Get S3 Metadata",  
                        kv("stackTrace", e.getStackTrace()), 
                        kv("service", APIModuleConstants.SERVICE_NAME),
                        kv("jobId", job.getId()),
                        kv("requestId", requestId));
        }

        return Collections.emptyMap();
    }

    private List<Map<String, Object>> getJsonData(JobDTO job, String requestId) {
        Map<String, String> params = job.getResultStoreParams();
        String bucket = params.get(BUCKET_NAME_KEY);
        String object = params.get(OBJECT_NAME_KEY);
        String objectPrefix = params.get(OBJECT_PREFIX_KEY);
        String metadataObject = params.get(METADATA_OBJECT_NAME_KEY);

        List<byte[]> allContents = new ArrayList<>();
        if (StringUtils.isNotEmpty(bucket)) {
            if (StringUtils.isNotEmpty(object)) {
                byte[] contents = s3Service.getObject(bucket, object, requestId);
                allContents.add(contents);
            } else if (StringUtils.isNotEmpty(objectPrefix)) {
                List<byte[]> contentsList = s3Service.getAllObjects(bucket, objectPrefix, requestId).stream()
                        .filter(objectInfoPair -> !objectInfoPair.getFirst().equals(metadataObject))
                        .map(Pair::getSecond)
                        .collect(Collectors.toList());

                allContents.addAll(contentsList);
            }
        }

        return allContents.stream()
                .flatMap(data -> {
                    try {
                        List<Map<String,Object>> maps = new ArrayList<>();
                        JsonFactory jf = new JsonFactory();
                        JsonParser jp = jf.createParser(data);
                        jp.setCodec(objectMapper);
                        jp.nextToken();
                        while (jp.hasCurrentToken()) {
                            maps.add(jp.readValueAs(OBJ_MAP_TYPEREF));
                            jp.nextToken();
                        }
                        return maps.stream();
                    } catch (IOException e) {
                        LOGGER.error("FAILED To Get Json Data",  
                                    kv("stackTrace", e.getStackTrace()), 
                                    kv("service", APIModuleConstants.SERVICE_NAME),
                                    kv("jobId", job.getId()),
                                    kv("requestId", requestId));
                    }

                    return Stream.empty();
                })
                .filter(m -> !m.isEmpty())
                .collect(Collectors.toList());
    }

    private List<String> getTextData(JobDTO job, String requestId) {
        Map<String, String> params = job.getResultStoreParams();
        String bucket = params.get(BUCKET_NAME_KEY);
        String object = params.get(OBJECT_NAME_KEY);
        String objectPrefix = params.get(OBJECT_PREFIX_KEY);
        String metadataObject = params.get(METADATA_OBJECT_NAME_KEY);

        List<byte[]> allContents = new ArrayList<>();
        if (StringUtils.isNotEmpty(bucket)) {
            if (StringUtils.isNotEmpty(object)) {
                byte[] contents = s3Service.getObject(bucket, object, requestId);
                allContents.add(contents);
            } else if (StringUtils.isNotEmpty(objectPrefix)) {
                List<byte[]> contentsList = s3Service.getAllObjects(bucket, objectPrefix, requestId).stream()
                        .filter(objectInfoPair -> !objectInfoPair.getFirst().equals(metadataObject))
                        .map(Pair::getSecond)
                        .collect(Collectors.toList());

                allContents.addAll(contentsList);
            }
        }

        return allContents.stream()
                .map(data -> new String(data, StandardCharsets.UTF_8))
                .collect(Collectors.toList());
    }
}
