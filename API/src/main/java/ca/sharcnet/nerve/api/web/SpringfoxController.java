package ca.sharcnet.nerve.api.web;

import ca.sharcnet.nerve.api.APIModuleConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UncheckedIOException;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * This controller serves the files needed for the Swagger contract endpoint.
 * 
 * While this is usually automatically configured with the Spring/Swagger
 * integration, it was not generating the API contracts correctly for some
 * of our endpoints. Therefore, instead of auto-generating the contract, the
 * contract itself is maintained in src/main/resources/api/nssi-api.json
 * and this controller loads and serves that static file and anything else that
 * the swagger view needs to render the contract.
 */

@Profile(APIModuleConstants.PROFILE_NAME)
@RestController
public class SpringfoxController {
    private static final String swaggerResources = "[\n" +
            "    {\n" +
            "        \"name\": \"default\",\n" +
            "        \"url\": \"/v3/api-docs\",\n" +
            "        \"swaggerVersion\": \"3.0.3\",\n" +
            "        \"location\": \"/v3/api-docs\"\n" +
            "    }\n" +
            "]";

    private static final String uiConfiguration = "{\n" +
            "    \"deepLinking\": true,\n" +
            "    \"displayOperationId\": true,\n" +
            "    \"defaultModelsExpandDepth\": 1,\n" +
            "    \"defaultModelExpandDepth\": 1,\n" +
            "    \"defaultModelRendering\": \"example\",\n" +
            "    \"displayRequestDuration\": false,\n" +
            "    \"docExpansion\": \"none\",\n" +
            "    \"filter\": false,\n" +
            "    \"operationsSorter\": \"alpha\",\n" +
            "    \"showExtensions\": false,\n" +
            "    \"showCommonExtensions\": false,\n" +
            "    \"tagsSorter\": \"alpha\",\n" +
            "    \"validatorUrl\": \"\",\n" +
            "    \"supportedSubmitMethods\": [\n" +
            "        \"get\",\n" +
            "        \"put\",\n" +
            "        \"post\",\n" +
            "        \"delete\",\n" +
            "        \"options\",\n" +
            "        \"head\",\n" +
            "        \"patch\",\n" +
            "        \"trace\"\n" +
            "    ]\n" +
            "}";

    private static final String securityConfiguration = "{\n" +
            "    \"clientId\": \"nssi\",\n" +
            "    \"clientSecret\": \"password\",\n" +
            "    \"realm\": \"nerve_client\",\n" +
            "    \"appName\": \"NSSI\",\n" +
            "    \"scopeSeparator\": \",\",\n" +
            "    \"useBasicAuthenticationWithAccessCodeGrant\": false\n" +
            "}";

    private ResourceLoader resourceLoader;

    @Autowired
    SpringfoxController(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    @GetMapping(value = "/v3/api-docs", produces = {"application/json", "application/hal+json"})
    public String getApiContract() {
        Resource contract = resourceLoader.getResource("classpath:api/nssi-api.json");

        try (Reader reader = new InputStreamReader(contract.getInputStream(), UTF_8)) {
            return FileCopyUtils.copyToString(reader);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    @GetMapping(value = "/swagger-resources", produces = {"application/json"})
    public String getSwaggerResources() {
        return swaggerResources;
    }

    @GetMapping(value = "/swagger-resources/configuration/ui", produces = {"application/json"})
    public String getUiConfiguration() {
        return uiConfiguration;
    }

    @GetMapping(value = "/swagger-resources/configuration/security", produces = {"application/json"})
    public String getSecurityConfigurations() {
        return securityConfiguration;
    }
}
