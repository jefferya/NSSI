package ca.sharcnet.nerve.api.repository;

import ca.sharcnet.nerve.api.APIModuleConstants;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Profile(APIModuleConstants.PROFILE_NAME)
@Repository
public interface UploadRepository extends JpaRepository<Upload, Long> {
}
