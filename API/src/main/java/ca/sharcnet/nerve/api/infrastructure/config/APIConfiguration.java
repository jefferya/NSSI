package ca.sharcnet.nerve.api.infrastructure.config;

import ca.sharcnet.nerve.api.APIModuleConstants;
import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Profile(APIModuleConstants.PROFILE_NAME)
@Configuration
public class APIConfiguration {

    private static final String ADMIN_CLIENT = "admin-cli";

    @Bean
    public Keycloak keycloakAdminClient(@Value("${keycloak.auth-server-url}") String authServerUrl,
                                        @Value("${keycloak.realm}") String authRealm,
                                        @Value("${nssi.keycloak.admin-client.secret}") String adminClientSecret)
    {
        return KeycloakBuilder.builder()
                .serverUrl(authServerUrl)
                .realm(authRealm)
                .grantType(OAuth2Constants.CLIENT_CREDENTIALS)
                .clientId(ADMIN_CLIENT)
                .clientSecret(adminClientSecret)
                .build();
    }
}
