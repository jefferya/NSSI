package ca.sharcnet.nerve.standoff.service;

import ca.sharcnet.nerve.standoff.model.Annotation;
import ca.sharcnet.nerve.standoff.model.ClassifiedEntity;
import lombok.Builder;
import lombok.Data;
import lombok.Singular;

import java.util.List;
import java.util.Optional;

@Data
@Builder(toBuilder = true)
public class StandoffDocument {
    /**
     * The standoff annotations (i.e., XML elements) contained within
     * the document.
     */
    private List<Annotation> annotations;

    /**
     * The entities contained within the document.
     */
    @Singular
    private List<ClassifiedEntity> taggedEntities;

    /**
     * The plaintext version of the document.
     */
    private String plain;

    /**
     * Returns the first {@code Annotation} whose label matches {@code label}, if it exists.
     *
     * @param label the annotation label to search for
     */
    public Optional<Annotation> findFirst(String label) {
        return annotations.stream()
                .filter(annotation -> label.equals(annotation.getLabel()))
                .findFirst();
    }
}
