package ca.sharcnet.nerve.standoff.service;

import ca.sharcnet.nerve.standoff.model.Annotation;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.*;

@Service
public class StandoffConverter {
    /**
     * Converts an XML (TEI) document to standoff format.
     *
     * @param tei the TEI/XML document to convert
     */
    public StandoffDocument toStandoff(Document tei) {
        Node root = tei.getDocumentElement();

        NodeList children = root.getChildNodes();
        String plain = root.getTextContent();
        List<Annotation> annotations = new ArrayList<>();
        annotations.add(toStandoffNode(0, 0, root));
        annotations.addAll(toStandoffChildren(children, 1, 0));

        return StandoffDocument.builder()
                .annotations(annotations)
                .plain(plain)
                .build();
    }

    /**
     * Returns a map containing the attributes on a node, mapped to their values.
     *
     * @param node the element to get attributes of
     */
    private Map<String, String> getAttributes(Node node) {
        NamedNodeMap attributes = node.getAttributes();
        if (Objects.isNull(attributes)) {
            return Collections.emptyMap();
        }

        Map<String, String> result = new HashMap<>();
        for (int j = 0; j < attributes.getLength(); j++) {
            Node attr = attributes.item(j);
            result.put(attr.getNodeName(), attr.getNodeValue());
        }

        return result;
    }

    /**
     * Converts a single element to a standoff {@code Annotation}.
     *
     * @param start the character start position of the text contained in {@code node}
     * @param depth the node depth of {@code node} within the XML document
     * @param node the element node
     */
    private Annotation toStandoffNode(int start, int depth, Node node) {
        return Annotation.builder()
                .start(start)
                .end(start + node.getTextContent().length())
                .depth(depth)
                .label(node.getNodeName())
                .attributes(getAttributes(node))
                .build();
    }

    /**
     * Recursively converts a list of nodes to standoff {@code Annotation}s.
     *
     * @param nodes the list of nodes to convert
     * @param depth the node depth of the {@code nodes} within the XML document
     * @param parentStart the character start position of the text contained in the
     *                    parent element of {@code nodes}
     */
    private List<Annotation> toStandoffChildren(NodeList nodes, int depth, int parentStart) {
        int start = parentStart;
        List<Annotation> result = new ArrayList<>();

        for (int i = 0; i < nodes.getLength(); i++) {
            Node child = nodes.item(i);

            if (child.getNodeType() == Node.TEXT_NODE) {
                start += child.getTextContent().length();
                continue;
            }

            result.add(toStandoffNode(start, depth, child));

            if (child.hasChildNodes()) {
                result.addAll(toStandoffChildren(child.getChildNodes(), depth + 1, start));
            }

            start += child.getTextContent().length();
        }

        return result;
    }
}
