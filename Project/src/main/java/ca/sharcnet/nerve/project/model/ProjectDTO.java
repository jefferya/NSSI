package ca.sharcnet.nerve.project.model;

import lombok.*;

import java.util.List;

/**
 * Data-transfer class for projects.
 */

@Data
@Builder(builderClassName = "Builder")
@AllArgsConstructor
@NoArgsConstructor
public class ProjectDTO {
    /**
     * The project ID.
     */
    private Long id;

    /**
     * The project name.
     */
    private String projectName;

    /**
     * The resources that comprise the project.
     */
    @Singular
    private List<ProjectResourceDTO> resources;
}
