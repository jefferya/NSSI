package ca.sharcnet.nerve.project.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Data-transfer class for project resources.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProjectResourceDTO {
    /**
     * The resource ID.
     */
    private String id;

    /**
     * The source document URI.
     */
    private String sourceDocumentUri;

    /**
     * The results URI.
     */
    private String resultsUri;
}
