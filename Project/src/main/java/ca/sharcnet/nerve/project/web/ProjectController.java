package ca.sharcnet.nerve.project.web;

import ca.sharcnet.nerve.apiexception.infrastructure.exception.ResourceNotFoundException;
import ca.sharcnet.nerve.project.ProjectModuleConstants;
import ca.sharcnet.nerve.project.model.ProjectDTO;
import ca.sharcnet.nerve.project.model.ProjectResourceDTO;
import ca.sharcnet.nerve.project.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

/**
 * This controller provides endpoints related to working with projects in NSSI.
 */
 
 // TODO: need request IDs + logging in controller methods
@Profile(ProjectModuleConstants.PROFILE_NAME)
@RestController
public class ProjectController {

    private final ProjectService projectService;

    @Autowired
    ProjectController(ProjectService projectService) {
        this.projectService = projectService;
    }
    
    /**
     * Returns a list of all available projects.
     * 
     * @param principal automatically set by Spring, this object represents the authenticated user
     * @param authToken the auth token for the request
     */
    @GetMapping(value = "/api/projects")
    public List<ProjectInfo> getProjectsList(Principal principal,
                                            @RequestHeader("Authorization") String authToken)
    {
        return projectService.getProjectsList().stream()
                .map(projectDTO -> ProjectInfo.builder()
                        .projectId(projectDTO.getId())
                        .projectName(projectDTO.getProjectName())
                        .projectResources(projectDTO.getResources().stream()
                                .map(projectResourceDTO -> ProjectResourceInfo.builder()
                                        .projectResourceId(projectResourceDTO.getId())
                                        .sourceDocumentUri(projectResourceDTO.getSourceDocumentUri())
                                        .resultsUri(projectResourceDTO.getResultsUri())
                                        .build())
                                .collect(Collectors.toList()))
                        .build())
                .collect(Collectors.toList());
    }

    /**
     * Creates a new project.
     * 
     * @param principal automatically set by Spring, this object represents the authenticated user
     * @param authToken the auth token for the request
     * @param projectInfo information about the new project (see {@link ca.sharcnet.nerve.project.web.ProjectCreationRequest})
     */
    @PostMapping(value = "/api/projects")
    public ResponseEntity<ProjectInfo> createProject(Principal principal,
                                                     @RequestHeader("Authorization") String authToken,
                                                     @RequestBody ProjectCreationRequest projectInfo)
    {
        ProjectDTO projectDTO = projectService.newProject(projectInfo.getProjectName());

        return new ResponseEntity<>(ProjectInfo.builder()
                .projectId(projectDTO.getId())
                .projectName(projectDTO.getProjectName())
                .build(),
                HttpStatus.CREATED
        );
    }

    /**
     * Returns the specified project.
     * 
     * @param principal automatically set by Spring, this object represents the authenticated user
     * @param authToken the auth token for the request
     * @param projectId the project ID
     */
    @GetMapping(value = "/api/projects/{projectId}")
    public ResponseEntity<ProjectInfo> getProjectInfo(Principal principal,
                                                      @RequestHeader("Authorization") String authToken,
                                                      @PathVariable("projectId") Long projectId)
            throws ResourceNotFoundException
    {
        ProjectDTO projectDTO = projectService.getProject(projectId).orElseThrow(ResourceNotFoundException::new);

        return new ResponseEntity<>(
                ProjectInfo.builder()
                        .projectId(projectDTO.getId())
                        .projectName(projectDTO.getProjectName())
                        .projectResources(
                                projectDTO.getResources().stream()
                                        .map(projectResource -> ProjectResourceInfo.builder()
                                                .projectResourceId(projectResource.getId())
                                                .sourceDocumentUri(projectResource.getSourceDocumentUri())
                                                .resultsUri(projectResource.getResultsUri())
                                                .build()
                                        )
                                        .collect(Collectors.toList())
                        )
                        .build(),
                HttpStatus.OK
        );
    }

    /**
     * Deletes the specified project.
     * 
     * @param principal automatically set by Spring, this object represents the authenticated user
     * @param authToken the auth token for the request
     * @param projectId the project ID
     */
    @DeleteMapping(value = "/api/projects/{projectId}")
    public ResponseEntity deleteProject(Principal principal,
                                        @RequestHeader("Authorization") String authToken,
                                        @PathVariable("projectId") Long projectId)
    {
        projectService.deleteProject(projectId);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    /**
     * Returns a list of resources in the given project.
     * 
     * @param principal automatically set by Spring, this object represents the authenticated user
     * @param authToken the auth token for the request
     * @param projectId the project ID
     */
    @GetMapping(value = "/api/projects/{projectId}/resources")
    public ResponseEntity<List<ProjectResourceInfo>> getProjectResourceList(Principal principal,
                                                                            @RequestHeader("Authorization") String authToken,
                                                                            @PathVariable("projectId") Long projectId)
            throws ResourceNotFoundException
    {
        ProjectDTO projectDTO = projectService.getProject(projectId).orElseThrow(ResourceNotFoundException::new);

        return new ResponseEntity<>(projectDTO.getResources().stream()
                .map(projectResource -> ProjectResourceInfo.builder()
                        .projectResourceId(projectResource.getId())
                        .sourceDocumentUri(projectResource.getSourceDocumentUri())
                        .resultsUri(projectResource.getResultsUri())
                        .build()
                )
                .collect(Collectors.toList()),
                HttpStatus.OK
        );
    }

    /**
     * Creates a new resource in the given project.
     * 
     * @param principal automatically set by Spring, this object represents the authenticated user
     * @param authToken the auth token for the request
     * @param projectId the project ID
     * @param resourceInfo information about the new resource (see {@link ca.sharcnet.nerve.project.web.ProjectResourceCreationRequest})
     */
    @PostMapping(value = "/api/projects/{projectId}/resources")
    public ResponseEntity<ProjectResourceInfo> createResourceInProject(Principal principal,
                                                                       @RequestHeader("Authorization") String authToken,
                                                                       @PathVariable("projectId") Long projectId,
                                                                       @RequestBody ProjectResourceCreationRequest resourceInfo)
            throws ResourceNotFoundException
    {
        try {
            ProjectResourceDTO resourceDTO = projectService.createProjectResource(
                    projectId, resourceInfo.getSourceDocumentUri(), resourceInfo.getResultsUri()
            );

            return new ResponseEntity<>(ProjectResourceInfo.builder()
                    .projectResourceId(resourceDTO.getId())
                    .sourceDocumentUri(resourceDTO.getSourceDocumentUri())
                    .resultsUri(resourceDTO.getResultsUri())
                    .build(),
                    HttpStatus.CREATED
            );
        } catch (NoSuchElementException e) {
            throw new ResourceNotFoundException();
        }
    }

    /**
     * Returns the specified resource.
     * 
     * @param principal automatically set by Spring, this object represents the authenticated user
     * @param authToken the auth token for the request
     * @param projectId the project ID
     * @param resourceId the resource ID
     */
    @GetMapping(value = "/api/projects/{projectId}/resources/{resourceId}")
    public ResponseEntity<ProjectResourceInfo> getProjectResource(Principal principal,
                                                                  @RequestHeader("Authorization") String authToken,
                                                                  @PathVariable("projectId") Long projectId,
                                                                  @PathVariable("resourceId") String resourceId)
    {
        ProjectResourceDTO resourceDTO = projectService.getProjectResource(projectId, resourceId)
                .orElseThrow(ResourceNotFoundException::new);

        return new ResponseEntity<>(ProjectResourceInfo.builder()
                .projectResourceId(resourceDTO.getId())
                .sourceDocumentUri(resourceDTO.getSourceDocumentUri())
                .resultsUri(resourceDTO.getResultsUri())
                .build(),
                HttpStatus.OK
        );
    }

    /**
     * Deletes the given project resource.
     * 
     * @param principal automatically set by Spring, this object represents the authenticated user
     * @param authToken the auth token for the request
     * @param projectId the project ID
     * @param resourceId the resource ID
     */
    @DeleteMapping(value = "/api/projects/{projectId}/resources/{resourceId}")
    public ResponseEntity deleteProjectResource(Principal principal,
                                                @RequestHeader("Authorization") String authToken,
                                                @PathVariable("projectId") Long projectId,
                                                @PathVariable("resourceId") String resourceId)
    {
        projectService.deleteProjectResource(projectId, resourceId);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
