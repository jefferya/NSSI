package ca.sharcnet.nerve.project.infrastructure.config;

import ca.sharcnet.nerve.project.ProjectModuleConstants;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Profile(ProjectModuleConstants.PROFILE_NAME)
@Configuration
public class ProjectConfiguration {
}
