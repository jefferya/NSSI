package ca.sharcnet.nerve.project.web;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProjectResourceInfo {
    /**
     * The resource ID.
     */
    private String projectResourceId;

    /**
     * The source document URI.
     */
    private String sourceDocumentUri;

    /**
     * The results URI.
     */
    private String resultsUri;
}
