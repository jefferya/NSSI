package ca.sharcnet.nerve.project.web;

import lombok.*;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProjectInfo {
    /**
     * The project ID.
     */
    private Long projectId;

    /**
     * The project name.
     */
    private String projectName;

    /**
     * The resources that make up the project.
     */
    @Singular
    private List<ProjectResourceInfo> projectResources;
}
