package ca.sharcnet.nerve.s3sink.infrastructure.config;

import ca.sharcnet.nerve.s3sink.S3SinkModuleConstants;
import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import static ca.sharcnet.nerve.broker.infrastructure.config.RMQConfiguration.*;
import static ca.sharcnet.nerve.s3sink.S3SinkModuleConstants.QUEUE_NAME;

@Profile(S3SinkModuleConstants.PROFILE_NAME)
@Configuration
public class S3SinkConfiguration {
    @Bean
    public Queue s3SinkQueue() {
        return QueueBuilder.durable(QUEUE_NAME)
                .withArgument(DLQ_KEY, DLX_APP_MESSAGES)
                .build();
    }

    @Bean
    public Binding s3SinkBinding(@Qualifier(APP_MESSAGES_EXCHANGE) DirectExchange exchange) {
        return BindingBuilder.bind(s3SinkQueue()).to(exchange)
                .with(QUEUE_NAME);
    }
}
