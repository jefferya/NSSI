package ca.sharcnet.nerve.s3sink.service;

import ca.sharcnet.nerve.broker.message.NSSIServiceMessage;
import ca.sharcnet.nerve.broker.service.RMQUtilsService;
import ca.sharcnet.nerve.s3sink.S3SinkModuleConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Profile(S3SinkModuleConstants.PROFILE_NAME)
@Service
public class S3SinkListenerService {
    private final S3SinkService s3SinkService;
    private final RMQUtilsService rmqUtils;
    private final Logger LOGGER = LoggerFactory.getLogger(S3SinkListenerService.class);

    @Autowired
    S3SinkListenerService(S3SinkService s3SinkService,
                          RMQUtilsService rmqUtils)
    {
        this.s3SinkService = s3SinkService;
        this.rmqUtils = rmqUtils;
    }

    @RabbitListener(queues = S3SinkModuleConstants.QUEUE_NAME)
    public void listen(Message message) {
        rmqUtils.processWithRetry(message, NSSIServiceMessage.class, (s3SinkMessage -> {

            LOGGER.info("S3SINK Received message for job ID " + s3SinkMessage.getJobId());
            String document = s3SinkMessage.getDocument();
            s3SinkService.saveDocument(s3SinkMessage.getJobId(), s3SinkMessage.getRequestId(), document);
        }));
    }
}
