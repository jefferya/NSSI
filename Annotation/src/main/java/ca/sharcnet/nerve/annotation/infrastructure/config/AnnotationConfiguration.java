package ca.sharcnet.nerve.annotation.infrastructure.config;

import ca.sharcnet.nerve.annotation.AnnotationModuleConstants;
import ca.sharcnet.nerve.elucidate_api.model.AnnotationContext;
import ca.sharcnet.nerve.elucidate_api.model.AnnotationCreator;
import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.util.Map;

import static ca.sharcnet.nerve.annotation.AnnotationModuleConstants.QUEUE_NAME;
import static ca.sharcnet.nerve.annotation.service.AnnotationConstants.*;
import static ca.sharcnet.nerve.broker.infrastructure.config.RMQConfiguration.*;

@Profile(AnnotationModuleConstants.PROFILE_NAME)
@Configuration
public class AnnotationConfiguration {

    @Bean
    public AnnotationContext annotationContext() {
        return AnnotationContext.builder()
                .dctermsCreatedContext(Map.of(
                        "@type", "xsd:dateTime",
                        "@id", "dcterms:created"
                ))
                .dctermsIssuedContext(Map.of(
                        "@type", "xsd:dateTime",
                        "@id", "dcterms:issued"
                ))
                .oaMotivatedByContext(Map.of(
                        "@type", MOTIVATION_TYPE
                ))
                .language(LANGUAGE_EN)
                .rdfContext(RDF)
                .rdfSchemaContext(RDFS)
                .activityStreamsContext(AS)
                .cwrcContext(CWRC)
                .bibframeContext(BF)
                .dcContext(DC)
                .dctermsContext(DCTERMS)
                .oaContext(OA)
                .schemaContext(SCHEMA)
                .xsdContext(XSD)
                .build();
    }

    @Bean
    public AnnotationCreator annotationCreator() {
        return AnnotationCreator.builder()
                .id("http://localhost:8080")
                .type(APPLICATION_TYPE)
                .label("NSSI Local Instance")
                .softwareVersion("v2.0.0")
                .build();
    }

    @Bean
    public Queue annotationQueue() {
        return QueueBuilder.durable(QUEUE_NAME)
                .withArgument(DLQ_KEY, DLX_APP_MESSAGES)
                .build();
    }

    @Bean
    public Binding annotationBinding(@Qualifier(APP_MESSAGES_EXCHANGE) DirectExchange exchange) {
        return BindingBuilder.bind(annotationQueue()).to(exchange)
                .with(QUEUE_NAME);
    }
}
