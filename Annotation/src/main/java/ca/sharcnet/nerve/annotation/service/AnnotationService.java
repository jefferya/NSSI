package ca.sharcnet.nerve.annotation.service;

import ca.sharcnet.nerve.annotation.AnnotationModuleConstants;
import ca.sharcnet.nerve.broker.message.*;
import ca.sharcnet.nerve.broker.service.RMQUtilsService;
import ca.sharcnet.nerve.elucidate_api.model.*;
import ca.sharcnet.nerve.elucidate_api.service.AnnotationClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import static ca.sharcnet.nerve.annotation.service.AnnotationConstants.*;
import static net.logstash.logback.argument.StructuredArguments.kv;

@Profile(AnnotationModuleConstants.PROFILE_NAME)
@Service
public class AnnotationService {

    private final AnnotationClient annotationClient;
    private final AnnotationContext annotationContext;
    private final AnnotationCreator annotationCreator;
    private final RMQUtilsService rmqUtils;

    private final Logger LOGGER = LoggerFactory.getLogger(AnnotationService.class);

    @Autowired
    public AnnotationService(AnnotationContext annotationContext,
                             AnnotationCreator annotationCreator,
                             AnnotationClient annotationClient,
                             RMQUtilsService rmqUtils)
    {
        this.annotationContext = annotationContext;
        this.annotationCreator = annotationCreator;
        this.annotationClient = annotationClient;
        this.rmqUtils = rmqUtils;
    }


    public Annotation createAnnotationForLinkedEntity(String documentURI,
                                                      DocumentMIMEType format,
                                                      LinkedEntityInfo info,
                                                      Selection selection)
    {
        Annotation annotation = Annotation.builder()
                .jsonContext(annotationContext)
                .type(ANNOTATION_TYPE)
                .creator(annotationCreator)
                .created(LocalDateTime.now())
                .issued(LocalDateTime.now())
                .motivatedBy("oa:identifying")
                .target(getAnnotationTarget(documentURI,
                        format.getMediaTypeName(),
                        info.getNamedEntityInfo().getEntity(),
                        selection))
                .body(AnnotationChoice.builder()
                        .type(CHOICE_TYPE)
                        .items(getAnnotationBodies(
                                info.getLinks(),
                                info.getNamedEntityInfo().getClassification()))
                        .build())
                .build();

        return annotation;
    }
    public void createAnnotationsForLinkedEntity(Long jobId,
                                                 String documentURI,
                                                 String resultsURI,
                                                 DocumentMIMEType format,
                                                 LinkedEntityInfo linkedEntityInfo,
                                                 String requestId)
    {
        for (NamedEntityInfo.SelectionWithLemma selection : linkedEntityInfo.getNamedEntityInfo().getSelections()) {
            Annotation annotation = createAnnotationForLinkedEntity(
                    documentURI, format, linkedEntityInfo, selection.getSelection()
            );

            annotationClient.addAnnotation(
                    resultsURI,
                    annotation,
                    requestId
            );

            LOGGER.info("Sending Progress Message",
                    kv("service", AnnotationModuleConstants.SERVICE_NAME),
                    kv("jobId", jobId),
                    kv("requestId", requestId));
            ProgressMessage progressMessage = ProgressMessage.builder()
                    .serviceName(AnnotationModuleConstants.SERVICE_NAME)
                    .jobId(jobId)
                    .requestId(requestId)
                    .numCompleted(1)
                    .build();

            rmqUtils.sendProgressMessage(progressMessage);
        }
    }

    public AnnotationContainer getAnnotationContainer(String containerId) {
        return AnnotationContainer.builder().build(); // massive TODO
    }

    private AnnotationTarget getAnnotationTarget(String documentURI,
                                                 String format,
                                                 String entityText,
                                                 Selection selection)
    {
        return AnnotationTarget.builder()
                .type(SPECIFIC_RESOURCE_TYPE)
                .source(documentURI)
                .format(format)
                .selector(RefinableTextQuoteSelector.builder()
                        .type(TEXT_QUOTE_SELECTOR_TYPE)
                        .exact(entityText)
                        .refinedBy(
                                TextPositionSelector.builder()
                                        .type(TEXT_POSITION_SELECTOR_TYPE)
                                        .start(selection.getStart())
                                        .end(selection.getEnd())
                                        .build())
                        .build())
                .build();
    }

    private List<AnnotationBody> getAnnotationBodies(List<LinkingServiceResult> links,
                                                     EntityClassification classification)
    {
        String type = Optional.ofNullable(classification)
                .map(c -> ENTITY_TYPE_MAP.getOrDefault(c, OWL_THING_TYPE))
                .orElse(OWL_THING_TYPE);

        if (Objects.isNull(links)) {
            return Collections.emptyList();
        }

        return links.stream()
                .map(LinkingServiceResult::getMatches)
                .filter(Objects::nonNull)
                .flatMap(List::stream)
                .map(linkDetails -> AnnotationBody.builder()
                        .type(type)
                        .id(linkDetails.getUri())
                        .format("text/plain")
                        .label(linkDetails.getHeading())
                        .description(linkDetails.getDescription())
                        .build())
                .collect(Collectors.toList());
    }
}
