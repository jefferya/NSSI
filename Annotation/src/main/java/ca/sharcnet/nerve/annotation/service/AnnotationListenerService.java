package ca.sharcnet.nerve.annotation.service;

import ca.sharcnet.nerve.annotation.AnnotationModuleConstants;
import ca.sharcnet.nerve.broker.message.LinkedEntityInfo;
import ca.sharcnet.nerve.broker.message.NSSIServiceMessage;
import ca.sharcnet.nerve.broker.service.RMQUtilsService;
import ca.sharcnet.nerve.job.service.WorkflowETAService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import static net.logstash.logback.argument.StructuredArguments.kv;

@Profile(AnnotationModuleConstants.PROFILE_NAME)
@Service
public class AnnotationListenerService {
    private final AnnotationService annotationService;
    private final RMQUtilsService rmqUtils;
    private final WorkflowETAService etaService;
    private final ObjectMapper objectMapper;
    private int numMessages = 0;

    private final Logger LOGGER = LoggerFactory.getLogger(AnnotationListenerService.class);

    @Autowired
    AnnotationListenerService(AnnotationService annotationService,
                              RMQUtilsService rmqUtils,
                              WorkflowETAService etaService,
                              ObjectMapper objectMapper)
    {
        this.annotationService = annotationService;
        this.rmqUtils = rmqUtils;
        this.etaService = etaService;
        this.objectMapper = objectMapper;
    }

    @RabbitListener(queues = AnnotationModuleConstants.QUEUE_NAME)
    public void listen(Message message) {
        numMessages = etaService.checkUpdateAverage(rmqUtils.numInQueue(AnnotationModuleConstants.QUEUE_NAME),
                numMessages,
                AnnotationModuleConstants.QUEUE_NAME);
        rmqUtils.processWithRetry(message, NSSIServiceMessage.class, (linkedEntityMessage -> {
            try {
                LinkedEntityInfo leInfo = objectMapper.readValue(linkedEntityMessage.getDocument(), LinkedEntityInfo.class);

                LOGGER.info("Processing Annotation Message",
                           kv("service", AnnotationModuleConstants.SERVICE_NAME),
                           kv("jobId", linkedEntityMessage.getJobId()),
                           kv("entity", leInfo.getNamedEntityInfo().getEntity()),
                           kv("requestId", linkedEntityMessage.getRequestId()));

                annotationService.createAnnotationsForLinkedEntity(
                        linkedEntityMessage.getJobId(),
                        linkedEntityMessage.getDocumentURI(),
                        linkedEntityMessage.getResultsUri(),
                        linkedEntityMessage.getFormat(),
                        leInfo,
                        linkedEntityMessage.getRequestId()
                );
            } catch (JsonProcessingException e) {
                LOGGER.error("FAILED To Process Annotation Message",  
                            kv("stackTrace", e.getStackTrace()), 
                            kv("service", AnnotationModuleConstants.SERVICE_NAME),
                            kv("jobId", linkedEntityMessage.getJobId()),
                            kv("requestId", linkedEntityMessage.getRequestId()));
            }
        }));
    }
}
