package ca.sharcnet.nerve.job.service;

import ca.sharcnet.nerve.job.model.JobStatus;
import ca.sharcnet.nerve.job.repository.Job;
import ca.sharcnet.nerve.job.repository.JobRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@ActiveProfiles("monitor")
@SpringBootTest
public class JobServiceTest {

    @MockBean
    private JobRepository jobRepository;

    @Autowired
    private JobService jobService;

    @Test
    public void jobStatuses() {
        Job inProgressJob = basicJob(1L);
        when(jobRepository.findById(eq(1L))).thenReturn(Optional.of(inProgressJob));

        Job completedJob = basicJob(2L);
        completedJob.setAllCompleted(true);
        when(jobRepository.findById(eq(2L))).thenReturn(Optional.of(completedJob));

        Job cancelledJob = basicJob(3L);
        cancelledJob.setCancelled(true);
        when(jobRepository.findById(eq(3L))).thenReturn(Optional.of(cancelledJob));

        Job failedJob = basicJob(4L);
        failedJob.setFailed(true);
        when(jobRepository.findById(eq(4L))).thenReturn(Optional.of(failedJob));

        JobStatus job1Status = jobService.getJob(1L).get().getStatus();
        assertEquals(JobStatus.IN_PROGRESS, job1Status);

        JobStatus job2Status = jobService.getJob(2L).get().getStatus();
        assertEquals(JobStatus.READY, job2Status);

        JobStatus job3Status = jobService.getJob(3L).get().getStatus();
        assertEquals(JobStatus.CANCELLED, job3Status);

        JobStatus job4Status = jobService.getJob(4L).get().getStatus();
        assertEquals(JobStatus.FAILED, job4Status);
    }

    private Job basicJob(Long id) {
        return Job.builder()
                .id(id)
                .createdBy("test")
                .startTime(LocalDateTime.now())
                .lastUpdatedTime(LocalDateTime.now())
                .numTotal(0)
                .numCompleted(0)
                .allCompleted(false)
                .cancelled(false)
                .failed(false)
                .build();
    }
}
