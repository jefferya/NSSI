package ca.sharcnet.nerve.job.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class JobRepositoryTest {

    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private JobRepository jobRepository;

    @Test
    public void incrementCounts() {
        Job job = basicJob();
        testEntityManager.persistAndFlush(job);

        jobRepository.incrementCounts(job.getId(), 1, 0, 0, 0);
        Optional<Job> incrementedJob1 = jobRepository.findById(job.getId());
        assertTrue(incrementedJob1.isPresent());
        assertEquals(1, (long) incrementedJob1.get().getNumTotal());
        assertEquals(0, (long) incrementedJob1.get().getNumCompleted());

        jobRepository.incrementCounts(job.getId(), 0, 1, 0, 0);
        Optional<Job> incrementedJob2 = jobRepository.findById(job.getId());
        assertTrue(incrementedJob2.isPresent());
        assertEquals(1, (long) incrementedJob2.get().getNumTotal());
        assertEquals(1, (long) incrementedJob2.get().getNumCompleted());
    }

    @Test
    public void updateStatuses() {
        Job job1 = basicJob();
        testEntityManager.persistAndFlush(job1);
        assertFalse(job1.getAllCompleted());

        jobRepository.markCompleted(job1.getId());
        Optional<Job> completedJob = jobRepository.findById(job1.getId());
        assertTrue(completedJob.isPresent());
        assertTrue(completedJob.get().getAllCompleted());

        Job job2 = basicJob();
        testEntityManager.persistAndFlush(job2);
        assertFalse(job2.getCancelled());

        jobRepository.markCancelled(job2.getId());
        Optional<Job> cancelledJob = jobRepository.findById(job2.getId());
        assertTrue(cancelledJob.isPresent());
        assertTrue(cancelledJob.get().getCancelled());

        Job job3 = basicJob();
        testEntityManager.persistAndFlush(job3);
        assertFalse(job3.getFailed());

        jobRepository.markFailed(job3.getId());
        Optional<Job> failedJob = jobRepository.findById(job3.getId());
        assertTrue(failedJob.isPresent());
        assertTrue(failedJob.get().getFailed());
    }

    @Test
    public void findNewlyCompleted() {
        Job job = basicJob();
        job.setNumTotal(42);
        testEntityManager.persistAndFlush(job);

        List<Job> jobs1 = jobRepository.findNewlyCompleted();
        assertEquals(0, jobs1.size());

        jobRepository.incrementCounts(job.getId(), 0, 42, 0, 0);
        List<Job> jobs2 = jobRepository.findNewlyCompleted();
        assertEquals(1, jobs2.size());
        assertEquals(job.getId(), jobs2.get(0).getId());
    }

    @Test
    public void findNewlyCompleted_noTally() {
        Job job = basicJob();
        testEntityManager.persistAndFlush(job);

        List<Job> jobs1 = jobRepository.findNewlyCompleted();
        assertEquals(0, jobs1.size());
    }

    private Job basicJob() {
        return Job.builder()
                .createdBy("test")
                .startTime(LocalDateTime.now())
                .lastUpdatedTime(LocalDateTime.now())
                .numTotal(0)
                .numCompleted(0)
                .allCompleted(false)
                .cancelled(false)
                .failed(false)
                .build();
    }
}
