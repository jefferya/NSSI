package ca.sharcnet.nerve.job.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AverageQueueRepository extends JpaRepository<AverageQueue, Long> {
    List<AverageQueue> findByQueueAndTierName(String queue, String tierName);
}