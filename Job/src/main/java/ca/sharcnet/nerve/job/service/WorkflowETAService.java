package ca.sharcnet.nerve.job.service;

import ca.sharcnet.nerve.job.repository.AverageQueue;
import ca.sharcnet.nerve.job.repository.AverageQueueRepository;
import ca.sharcnet.nerve.job.repository.WorkflowETA;
import ca.sharcnet.nerve.job.repository.WorkflowETARepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Hashtable;

@Service
@Transactional
public class WorkflowETAService {
    private final WorkflowETARepository workflowETARepo;
    private final AverageQueueRepository averageQueueRepo;

    public final int QUEUE_TIME_REFRESH = 10;
    private final String tierName;

    private final Hashtable<String, LocalDateTime> elapsedTime = new Hashtable<>();
    private final Hashtable<String, Boolean> activeTime = new Hashtable<>();

    @Autowired
    public WorkflowETAService(WorkflowETARepository workflowETARepo,
                              AverageQueueRepository averageQueueRepo,
                              @Value("${nssi.tier_name:default}") String tierName)
    {
        this.workflowETARepo = workflowETARepo;
        this.averageQueueRepo = averageQueueRepo;
        this.tierName = tierName;
    }

    public Double getWorkflowETA(String workflow) {
        return 5.0 * (Math.round((workflowETARepo.findByWorkflowAndTierName(workflow, tierName).get(0).getEta() / 60.0) / 5));
    }

    public void updateETA(String workflow, Double estimated) {
        WorkflowETA workflowETA = WorkflowETA.builder()
                .workflow(workflow)
                .eta(estimated)
                .tierName(tierName)
                .build();

        workflowETARepo.save(workflowETA);
    }

    public Double getAverageTime(String queue) {
        return averageQueueRepo.findByQueueAndTierName(queue, tierName).get(0).getAverageTime();
    }

    public void updateAverageQueue(String queue, Double average) {
        AverageQueue averageQueue = AverageQueue.builder()
                .queue(queue)
                .averageTime(average)
                .tierName(tierName)
                .build();

        averageQueueRepo.save(averageQueue);
    }

    public void calcAndUpdateAverage(String queue, LocalDateTime time) {
        Long diff = ChronoUnit.MILLIS.between(time, LocalDateTime.now());

        AverageQueue averageQueue = AverageQueue.builder()
                .queue(queue)
                .averageTime(QUEUE_TIME_REFRESH / (diff * 0.001))
                .tierName(tierName)
                .build();

        averageQueueRepo.save(averageQueue);
    }

    public int checkUpdateAverage(int numInQueue, int numMessages, String queue) {
        if (numInQueue > QUEUE_TIME_REFRESH && activeTime.containsKey(queue) && !activeTime.get(queue)) {
            activeTime.put(queue, true);
        } else if (!activeTime.containsKey(queue)) {
            activeTime.put(queue, false);
            return 0;
        }

        if (activeTime.get(queue)) {
            numMessages += 1;
            if (numMessages == 1) {
                elapsedTime.put(queue, LocalDateTime.now());
            } else if (numMessages == QUEUE_TIME_REFRESH) {
                numMessages = 0;
                calcAndUpdateAverage(queue, elapsedTime.get(queue));
                activeTime.put(queue, false);
            }
        }

        return numMessages;
    }
}