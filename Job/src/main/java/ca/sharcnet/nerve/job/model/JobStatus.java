package ca.sharcnet.nerve.job.model;

public enum JobStatus {
    IN_PROGRESS,
    READY,
    CANCELLED,
    FAILED
}
