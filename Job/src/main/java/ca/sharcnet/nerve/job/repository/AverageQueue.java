package ca.sharcnet.nerve.job.repository;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "averageQueue")
@IdClass(AverageQueueId.class)
public class AverageQueue {
    @Id
    private String queue;
    @Id
    private String tierName;
    private Double averageTime;
}