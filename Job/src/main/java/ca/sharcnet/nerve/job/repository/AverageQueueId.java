package ca.sharcnet.nerve.job.repository;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AverageQueueId implements Serializable {
    private String queue;
    private String tierName;
}
