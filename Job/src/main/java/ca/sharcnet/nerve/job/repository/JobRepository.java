package ca.sharcnet.nerve.job.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface JobRepository extends JpaRepository<Job, Long> {
    List<Job> findByResultsUri(String resultsUri);
    List<Job> findByAllCompletedTrue();

    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Query("update #{#entityName} j set j.numTotal = j.numTotal + :totalInc, j.numCompleted = j.numCompleted + :completedInc, j.numTransientFailures = j.numTransientFailures + :transientFailuresInc, j.numPermanentFailures = j.numPermanentFailures + :permanentFailuresInc, lastUpdatedTime = NOW() where j.id = :id")
    void incrementCounts(@Param("id") Long id,
                         @Param("totalInc") Integer totalInc,
                         @Param("completedInc") Integer completedInc,
                         @Param("transientFailuresInc") Integer transientFailuresInc,
                         @Param("permanentFailuresInc") Integer permanentFailuresInc);

    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Query("update #{#entityName} j set j.allCompleted = true, j.lastUpdatedTime = NOW() where j.id = :id")
    void markCompleted(@Param("id") Long id);
    
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Query("update #{#entityName} j set j.cancelled = true, j.lastUpdatedTime = NOW() where j.id = :id")
    void markCancelled(@Param("id") Long id);

    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Query("update #{#entityName} j set j.failed = true, j.lastUpdatedTime = NOW() where j.id = :id")
    void markFailed(@Param("id") Long id);

    @Query("select j from #{#entityName} j where j.allCompleted = false and j.cancelled = false and j.failed = false and j.numTransientFailures = 0 and j.numPermanentFailures = 0 and j.lastUpdatedTime <= :timestamp")
    List<Job> findNotUpdatedSince(@Param("timestamp") LocalDateTime timestamp);

    @Query("select j from #{#entityName} j where j.numTotal > 0 and j.numTotal = j.numCompleted and j.allCompleted = false")
    List<Job> findNewlyCompleted();

    @Query("select j from #{#entityName} j where j.numPermanentFailures > 0 and j.failed = false")
    List<Job> findNewlyFailed();
}
