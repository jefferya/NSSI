package ca.sharcnet.nerve.job.service;

import ca.sharcnet.nerve.job.JobModuleConstants;
import ca.sharcnet.nerve.job.model.JobDTO;
import ca.sharcnet.nerve.job.model.JobStatus;
import ca.sharcnet.nerve.job.repository.Job;
import ca.sharcnet.nerve.job.repository.JobRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static net.logstash.logback.argument.StructuredArguments.kv;

@Service
@Transactional
public class JobService {

    private JobRepository jobRepository;
    private ObjectMapper objectMapper;

    private final TypeReference<HashMap<String, String>> MAP_TYPEREF = new TypeReference<>() {
    };

    private final Logger LOGGER = LoggerFactory.getLogger(JobService.class);

    @Autowired
    JobService(JobRepository jobRepository,
               ObjectMapper objectMapper)
    {
        this.jobRepository = jobRepository;
        this.objectMapper = objectMapper;
    }

    public Long newJob(String user, Map<String, String> context, String workflow, String clientId, String requestId) {
        String contextJson = null;
        try {
            contextJson = objectMapper.writeValueAsString(context);
        } catch (JsonProcessingException e) {
            LOGGER.error("FAILED To Create New Job",
                    kv("stackTrace", e.getStackTrace()),
                    kv("service", JobModuleConstants.SERVICE_NAME),
                    kv("client", user),
                    kv("requestId", requestId));
        }

        Job job = Job.builder()
                .createdBy(user)
                .clientId(clientId)
                .requestId(requestId)
                .context(contextJson)
                .workflow(workflow)
                .startTime(LocalDateTime.now())
                .numTotal(0)
                .numCompleted(0)
                .numTransientFailures(0)
                .numPermanentFailures(0)
                .allCompleted(false)
                .cancelled(false)
                .failed(false)
                .lastUpdatedTime(LocalDateTime.now())
                .build();

        jobRepository.save(job);

        return job.getId();
    }

    public Optional<JobDTO> getJob(Long jobId) {
        return jobRepository.findById(jobId)
                .map(job -> toJobDTO(job, job.getRequestId()));
    }

    public void updateResultsUri(Long jobId, String resultsUri) {
        jobRepository.findById(jobId)
                .ifPresent(job -> {
                    job.setResultsUri(resultsUri);
                    jobRepository.save(job);
                });
    }

    public void updateProgressCounts(Long jobId,
                                     int numStarted,
                                     int numCompleted,
                                     int numTransientFailures,
                                     int numPermanentFailures)
    {
        jobRepository.findById(jobId)
                .filter(job -> nullOrFalse(job.getCancelled()) && nullOrFalse(job.getFailed()))
                .ifPresent(job -> {
                            jobRepository.incrementCounts(
                                    job.getId(),
                                    numStarted,
                                    numCompleted,
                                    numTransientFailures,
                                    numPermanentFailures
                            );
                        }
                );
    }

    public void updateStatus(Long jobId, boolean completed, boolean failed) {
        jobRepository.findById(jobId)
                .filter(job -> nullOrFalse(job.getCancelled()) && nullOrFalse(job.getFailed()))
                .ifPresent(job -> {
                    if (completed) {
                        jobRepository.markCompleted(job.getId());
                    } else if (failed) {
                        jobRepository.markFailed(job.getId());
                    }
                });
    }

    public void updateResultsInfo(Long jobId, String resultStoreParams) {
        jobRepository.findById(jobId)
                .filter(job -> nullOrFalse(job.getCancelled()) && nullOrFalse(job.getFailed()))
                .ifPresent(job -> {
                    job.setResultStoreParams(resultStoreParams);
                    jobRepository.save(job);
                });
    }

    public void cancelJob(Long jobId) {
        jobRepository.markCancelled(jobId);
    }

    public void deleteJob(Long jobId) {
        jobRepository.deleteById(jobId);
    }

    public List<JobDTO> getNewlyCompletedJobs() {
        return jobRepository.findNewlyCompleted().stream()
                .map(job -> toJobDTO(job, job.getRequestId()))
                .collect(Collectors.toList());
    }

    public List<JobDTO> getNewlyFailedJobs() {
        return jobRepository.findNewlyFailed().stream()
                .map(job -> toJobDTO(job, job.getRequestId()))
                .collect(Collectors.toList());
    }

    public List<JobDTO> getStalledJobs() {
        return jobRepository.findNotUpdatedSince(LocalDateTime.now().minusMinutes(30)).stream()
                .map(job -> toJobDTO(job, job.getRequestId()))
                .collect(Collectors.toList());
    }

    private JobDTO toJobDTO(Job job, String requestId) {
        Map<String, String> resultStoreParams = Collections.emptyMap();
        try {
            if (Objects.nonNull(job.getResultStoreParams())) {
                resultStoreParams = objectMapper.readValue(job.getResultStoreParams(), MAP_TYPEREF);
            }
        } catch (Exception e) {
            LOGGER.error("FAILED To Create JobDTO",
                    kv("stackTrace", e.getStackTrace()),
                    kv("service", JobModuleConstants.SERVICE_NAME),
                    kv("jobId", job.getId()),
                    kv("requestId", requestId));
        }

        return JobDTO.builder()
                .id(job.getId())
                .workflow(job.getWorkflow())
                .resultsUri(job.getResultsUri())
                .createdBy(job.getCreatedBy())
                .clientId(job.getClientId())
                .startTime(job.getStartTime())
                .lastUpdatedTime(job.getLastUpdatedTime())
                .resultStoreParams(resultStoreParams)
                .status(getStatus(job.getAllCompleted(), job.getCancelled(), job.getFailed()))
                .build();
    }

    private JobStatus getStatus(boolean allCompleted,
                                boolean cancelled,
                                boolean failed)
    {
        if (cancelled) {
            return JobStatus.CANCELLED;
        }

        if (failed) {
            return JobStatus.FAILED;
        }

        if (allCompleted) {
            return JobStatus.READY;
        }

        return JobStatus.IN_PROGRESS;
    }

    private boolean nullOrFalse(Boolean b) {
        return b == null || !b;
    }
}
