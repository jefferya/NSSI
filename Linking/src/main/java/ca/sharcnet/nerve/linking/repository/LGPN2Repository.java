package ca.sharcnet.nerve.linking.repository;

import ca.sharcnet.nerve.linking.LinkingModuleConstants;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Profile(LinkingModuleConstants.PROFILE_NAME)
@Repository
public class LGPN2Repository {

    @Autowired
    private RestTemplate restTemplate;

    private ObjectMapper mapper;

    private final String requestURIBase = "https://lookup.services.cwrc.ca/lgpn2/cgi-bin/lgpn_search.cgi" +
            "?name={name}&style=json";

    @Autowired
    public LGPN2Repository(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    public LGPN2Response getResult(String entity) throws JsonProcessingException {
        String requestURI = getRequestURI(entity);

        String response = restTemplate.getForEntity(getRequestURI(entity), String.class).getBody()
                .replaceFirst("lgpn\\(", "")
                .replaceFirst("\\);", "");

        return mapper.readValue(response, LGPN2Response.class);
    }

    private String getRequestURI(String entity) {
        return UriComponentsBuilder.fromUriString(requestURIBase)
                .buildAndExpand(entity)
                .toUriString();
    }
}
