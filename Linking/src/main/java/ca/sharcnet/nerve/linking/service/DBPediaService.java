package ca.sharcnet.nerve.linking.service;

import ca.sharcnet.nerve.broker.message.EntityLinkDetails;
import ca.sharcnet.nerve.broker.message.LinkingServiceResult;
import ca.sharcnet.nerve.linking.LinkingModuleConstants;
import ca.sharcnet.nerve.linking.repository.DBPediaRepository;
import ca.sharcnet.nerve.linking.repository.DBPediaResponse;
import ca.sharcnet.nerve.linking.repository.LinkingServiceName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@Profile(LinkingModuleConstants.PROFILE_NAME)
@Service(value = "dbpediaService")
public class DBPediaService implements AsyncLinkingService {

    private DBPediaRepository repository;

    @Autowired
    public DBPediaService(DBPediaRepository repository) {
        this.repository = repository;
    }

    @Override
    public LinkingServiceName getName() {
        return LinkingServiceName.DBPEDIA;
    }

    @Async
    @Override
    public CompletableFuture<Optional<LinkingServiceResult>> getLinks(String entity, String type)
    {
        DBPediaResponse response = repository.getResult(entity, type);

        List<EntityLinkDetails> matches = response.getResults().stream()
                .map(result -> EntityLinkDetails.builder()
                        .uri(result.getUri())
                        .heading(result.getLabel())
                        .description(result.getDescription())
                        .build()
                ).collect(Collectors.toList());

        return CompletableFuture.completedFuture(
                LinkingServiceResult.builder()
                        .serviceName(this.getName().getServiceName())
                        .serviceURI(this.getName().getUri())
                        .matches(matches)
                        .buildOptional()
        );
    }
}
