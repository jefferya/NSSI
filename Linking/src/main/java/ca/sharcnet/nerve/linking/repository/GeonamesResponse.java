package ca.sharcnet.nerve.linking.repository;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * A class modelling the response from the Geonames API.
 */

@Data
@NoArgsConstructor
public class GeonamesResponse implements Serializable {

    @Data
    @NoArgsConstructor
    public static class GeonamesEntity implements Serializable{
        private long geonameId;
        private String toponymName;
        private String fcodeName;
    }

    private List<GeonamesEntity> geonames;
}
