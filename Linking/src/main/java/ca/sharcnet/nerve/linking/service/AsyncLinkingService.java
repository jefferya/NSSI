package ca.sharcnet.nerve.linking.service;

import ca.sharcnet.nerve.broker.message.LinkingServiceResult;
import ca.sharcnet.nerve.linking.repository.LinkingServiceName;

import java.util.Optional;
import java.util.concurrent.CompletableFuture;

public interface AsyncLinkingService {
    LinkingServiceName getName();
    CompletableFuture<Optional<LinkingServiceResult>> getLinks(String entity, String type);
}
