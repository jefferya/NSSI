package ca.sharcnet.nerve.linking.infrastructure.config;

import ca.sharcnet.nerve.linking.LinkingModuleConstants;
import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.wikidata.wdtk.wikibaseapi.WikibaseDataFetcher;

import static ca.sharcnet.nerve.broker.infrastructure.config.RMQConfiguration.*;
import static ca.sharcnet.nerve.linking.LinkingModuleConstants.QUEUE_NAME;

@Profile(LinkingModuleConstants.PROFILE_NAME)
@Configuration
public class LinkingConfiguration {

    @Bean
    public WikibaseDataFetcher wikibaseDataFetcher() {
        return WikibaseDataFetcher.getWikidataDataFetcher();
    }

    @Bean
    public Queue linkingQueue() {
        return QueueBuilder.durable(QUEUE_NAME)
                .withArgument(DLQ_KEY, DLX_APP_MESSAGES)
                .build();
    }

    @Bean
    public Binding linkingBinding(@Qualifier(APP_MESSAGES_EXCHANGE) DirectExchange exchange) {
        return BindingBuilder.bind(linkingQueue()).to(exchange)
                .with(QUEUE_NAME);
    }
}
