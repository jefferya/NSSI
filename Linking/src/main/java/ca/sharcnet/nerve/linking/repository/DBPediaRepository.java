package ca.sharcnet.nerve.linking.repository;

import ca.sharcnet.nerve.broker.message.EntityClassification;
import ca.sharcnet.nerve.linking.LinkingModuleConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Profile(LinkingModuleConstants.PROFILE_NAME)
@Repository
public class DBPediaRepository {

    @Autowired
    private RestTemplate restTemplate;

    private final String requestURIBase = "https://lookup.services.cwrc.ca/dbpedia/api/search/" +
            "KeywordSearch?QueryClass={queryType}&MaxHits={maxResults}&QueryString={entity}";

    private long maxResults;

    public DBPediaRepository(@Value("${nssi.linking.dbpedia.maxResults:5}") long maxResults) {
        this.maxResults = maxResults;
    }

    public DBPediaResponse getResult(String entity, String type) {
        return restTemplate.getForObject(getRequestURI(entity, type), DBPediaResponse.class);
    }

    private String getRequestURI(String entity, String type) {
        String queryType = EntityClassification.LOCATION.name().equals(type)
                ? "place" : "person";
        return UriComponentsBuilder.fromUriString(requestURIBase)
                .buildAndExpand(queryType, maxResults, entity)
                .toUriString();
    }
}
