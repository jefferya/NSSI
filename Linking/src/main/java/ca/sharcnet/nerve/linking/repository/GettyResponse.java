package ca.sharcnet.nerve.linking.repository;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * A class modelling the response from the Getty API.
 */

@Data
@NoArgsConstructor
public class GettyResponse {

    @Data
    @NoArgsConstructor
    public static class GettyTypedValue {
        private String type;
        private String value;
    }

    @Data
    @NoArgsConstructor
    public static class GettyBinding {
        @JsonProperty("Subject")
        private GettyTypedValue subject;

        @JsonProperty("Term")
        private GettyTypedValue term;

        @JsonProperty("Descr")
        private GettyTypedValue description;
    }

    @Data
    @NoArgsConstructor
    public static  class GettyResult {
        private List<GettyBinding> bindings;
    }

    private GettyResult results;
}
