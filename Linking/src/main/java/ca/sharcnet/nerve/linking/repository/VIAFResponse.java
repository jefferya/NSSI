package ca.sharcnet.nerve.linking.repository;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * A class modelling the response from the VIAF API.
 */

@Data
@NoArgsConstructor
public class VIAFResponse implements Serializable {

    @Data
    @NoArgsConstructor
    public static class SearchRetrieveResponse implements Serializable {
        private String version;
        private int numberOfRecords;
        private String resultSetIdleTime;
        private List<Map<String, VIAFRecordResponse>> records;
    }

    private SearchRetrieveResponse searchRetrieveResponse;
}
