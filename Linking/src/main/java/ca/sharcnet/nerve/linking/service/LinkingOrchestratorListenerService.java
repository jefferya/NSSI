package ca.sharcnet.nerve.linking.service;

import ca.sharcnet.nerve.broker.message.LinkedEntityInfo;
import ca.sharcnet.nerve.broker.message.NSSIServiceMessage;
import ca.sharcnet.nerve.broker.message.NamedEntityInfo;
import ca.sharcnet.nerve.broker.service.RMQUtilsService;
import ca.sharcnet.nerve.job.service.WorkflowETAService;
import ca.sharcnet.nerve.linking.LinkingModuleConstants;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import static net.logstash.logback.argument.StructuredArguments.kv;

@Profile(LinkingModuleConstants.PROFILE_NAME)
@Service
public class LinkingOrchestratorListenerService {
    private final LinkingOrchestratorService linkingOrchestratorService;
    private final RMQUtilsService rmqUtils;
    private final WorkflowETAService etaService;
    private int numMessages = 0;

    private final ObjectMapper objectMapper;

    private final Logger LOGGER = LoggerFactory.getLogger(LinkingOrchestratorService.class);

    @Autowired
    LinkingOrchestratorListenerService(LinkingOrchestratorService linkingOrchestratorService,
                                       RMQUtilsService rmqUtils,
                                       WorkflowETAService etaService,
                                       ObjectMapper objectMapper)
    {
        this.linkingOrchestratorService = linkingOrchestratorService;
        this.rmqUtils = rmqUtils;
        this.etaService = etaService;
        this.objectMapper = objectMapper;
    }

    @RabbitListener(queues = LinkingModuleConstants.QUEUE_NAME)
    public void listen(Message serviceMessage) {
        numMessages = etaService.checkUpdateAverage(rmqUtils.numInQueue(LinkingModuleConstants.QUEUE_NAME),
                numMessages,
                LinkingModuleConstants.QUEUE_NAME);

        rmqUtils.processWithRetry(serviceMessage, NSSIServiceMessage.class, (this::processMessage));
    }

    private void processMessage(NSSIServiceMessage message) throws JsonProcessingException {
        try {
            NamedEntityInfo neInfo = objectMapper.readValue(message.getDocument(), NamedEntityInfo.class);
            LOGGER.info("Processing Linking Message",
                    kv("service", LinkingModuleConstants.SERVICE_NAME),
                    kv("jobId", message.getJobId()),
                    kv("entity", neInfo.getEntity()),
                    kv("type", neInfo.getClassification().name()),
                    kv("requestId", message.getRequestId()));


            LOGGER.info("Processing linking message for entity: " + neInfo.getEntity()
                    + ", of type: " + neInfo.getClassification().name());

            LinkedEntityInfo leInfo =
                    linkingOrchestratorService.getLinksForEntity(neInfo, message.getAuthorities());

            String leInfoDocument = objectMapper.writeValueAsString(leInfo);

            NSSIServiceMessage result = NSSIServiceMessage.builder()
                    .jobId(message.getJobId())
                    .projectName(message.getProjectName())
                    .pipeline(message.getPipeline())
                    .resultsUri(message.getResultsUri())
                    .document(leInfoDocument)
                    .documentURI(message.getDocumentURI())
                    .format(message.getFormat())
                    .requestId(message.getRequestId())
                    .build();

            rmqUtils.sendToNextStep(result);
        } catch (JsonProcessingException e) {
            LOGGER.error("FAILED To Process Linking Message",
                    kv("stackTrace", e.getStackTrace()),
                    kv("service", LinkingModuleConstants.SERVICE_NAME),
                    kv("jobId", message.getJobId()),
                    kv("requestId", message.getRequestId()));
        }
    }
}
