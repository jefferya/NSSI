package ca.sharcnet.nerve.linking.service;

import ca.sharcnet.nerve.broker.message.EntityLinkDetails;
import ca.sharcnet.nerve.broker.message.LinkingServiceResult;
import ca.sharcnet.nerve.linking.LinkingModuleConstants;
import ca.sharcnet.nerve.linking.repository.LinkingServiceName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.wikidata.wdtk.wikibaseapi.WbSearchEntitiesResult;
import org.wikidata.wdtk.wikibaseapi.WikibaseDataFetcher;
import org.wikidata.wdtk.wikibaseapi.apierrors.MediaWikiApiErrorException;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@Profile(LinkingModuleConstants.PROFILE_NAME)
@Service(value = "wikidataService")
public class WikidataService implements AsyncLinkingService {

    private WikibaseDataFetcher wikibaseDataFetcher;
    private long maxResults;

    @Autowired
    public WikidataService(WikibaseDataFetcher wikibaseDataFetcher,
                           @Value("${nssi.linking.wikidata.maxResults:5}") long maxResults) {
        this.wikibaseDataFetcher = wikibaseDataFetcher;
        this.maxResults = maxResults;
    }

    @Override
    public LinkingServiceName getName() {
        return LinkingServiceName.WIKIDATA;
    }

    @Async
    @Override
    public CompletableFuture<Optional<LinkingServiceResult>> getLinks(String entity, String type)
    {
        try {
            List<WbSearchEntitiesResult> searchResults = wikibaseDataFetcher.searchEntities(entity, maxResults);
            List<EntityLinkDetails> matches = searchResults.stream()
                    .map(result -> EntityLinkDetails.builder()
                            .uri(result.getConceptUri())
                            .heading(result.getLabel())
                            .description(result.getDescription())
                            .build()
                    ).collect(Collectors.toList());

            return CompletableFuture.completedFuture(
                    LinkingServiceResult.builder()
                            .serviceName(this.getName().getServiceName())
                            .serviceURI(this.getName().getUri())
                            .matches(matches)
                            .buildOptional()
            );

        } catch (MediaWikiApiErrorException | IOException e) {
            e.printStackTrace();
            return CompletableFuture.completedFuture(Optional.empty());
        }
    }
}
