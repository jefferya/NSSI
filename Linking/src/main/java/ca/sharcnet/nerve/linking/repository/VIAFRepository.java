package ca.sharcnet.nerve.linking.repository;

import ca.sharcnet.nerve.broker.message.EntityClassification;
import ca.sharcnet.nerve.linking.LinkingModuleConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;

import static net.logstash.logback.argument.StructuredArguments.kv;

@Profile(LinkingModuleConstants.PROFILE_NAME)
@Repository
public class VIAFRepository {

    @Autowired
    private RestTemplate restTemplate;

    private final String requestURIBase = "https://viaf.org/viaf/search?query={queryType}%20all%20{entity}" +
            "&httpAccept=application/json&maximumRecords={maxResults}";

    private long maxResults;

    private final Logger LOGGER = LoggerFactory.getLogger(VIAFRepository.class);

    public VIAFRepository(@Value("${nssi.linking.viaf.maxResults:5}") long maxResults) {
        this.maxResults = maxResults;
    }

    @Cacheable("viafEntities")
    public VIAFResponse getResult(String entity, String type) {
        LOGGER.info("Sending VIAF Request",  
                   kv("service", LinkingModuleConstants.SERVICE_NAME),
                   kv("entity", entity),
                   kv("type", type));
        return restTemplate.getForObject(getRequestURI(entity, type), VIAFResponse.class);
    }

    private String getRequestURI(String entity, String type) {
        String queryType = EntityClassification.LOCATION.name().equals(type)
                ? "local.geographicNames" : "local.personalNames";
        String encodedURI = UriComponentsBuilder.fromUriString(requestURIBase)
                        .buildAndExpand(queryType, entity, maxResults)
                        .toUriString();
        try {
            return URLDecoder.decode(encodedURI, StandardCharsets.UTF_8.name());
        } catch (UnsupportedEncodingException e) {
            return encodedURI;
        }
    }
}
