package ca.sharcnet.nerve.linking.repository;

import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.Nulls;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.wikidata.wdtk.wikibaseapi.WbSearchEntitiesResult;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class TestData {

    private static final ObjectMapper objectMapper;

    static {
        objectMapper = new ObjectMapper()
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
                .setDefaultSetterInfo(JsonSetter.Value.forContentNulls(Nulls.AS_EMPTY))
                .registerModule(new JavaTimeModule());
    }

    public static VIAFResponse viafPersonResponse() throws IOException {
        return getResponse("mockData/viaf/mclaren.json", VIAFResponse.class);
    }

    public static VIAFResponse viafLocationResponse() throws IOException {
        return getResponse("mockData/viaf/toronto.json", VIAFResponse.class);
    }

    public static GeonamesResponse geonamesResponse() throws IOException {
        return getResponse("mockData/geonames/toronto.json", GeonamesResponse.class);
    }

    public static List<WbSearchEntitiesResult> wikidataLocationResponse() throws IOException {
        List<WbSearchEntitiesResultTestImpl> entities = getResponse("mockData/wikidata/toronto.json", new TypeReference<>() {
        });

        return new ArrayList<>(entities);
    }

    private static <T> T getResponse(String filePath, Class<T> deserializationType) throws IOException {
        URL fileUrl = TestData.class.getClassLoader().getResource(filePath);
        T result = objectMapper.readValue(fileUrl, deserializationType);

        return result;
    }

    private static <T> T getResponse(String filePath, TypeReference<T> deserializationType) throws IOException {
        URL fileUrl = TestData.class.getClassLoader().getResource(filePath);
        T result = objectMapper.readValue(fileUrl, deserializationType);

        return result;
    }
}
