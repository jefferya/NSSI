package ca.sharcnet.nerve.linking.service;

import ca.sharcnet.nerve.linking.repository.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.wikidata.wdtk.wikibaseapi.WikibaseDataFetcher;
import org.wikidata.wdtk.wikibaseapi.apierrors.MediaWikiApiErrorException;

import java.io.IOException;

@RunWith(SpringRunner.class)
@ActiveProfiles("linking")
@SpringBootTest
public class LinkingOrchestratorServiceTest {

    @Autowired
    private LinkingOrchestratorService linkingOrchestratorService;

    @MockBean
    private LinkingOrchestratorListenerService listenerService;

    @MockBean
    private VIAFRepository viafRepository;

    @MockBean
    private DBPediaRepository dbPediaRepository;

    @MockBean
    private GeonamesRepository geonamesRepository;

    @MockBean
    private GettyRepository gettyRepository;

    @MockBean
    private LGPN2Repository lgpn2Repository;

    @MockBean
    private WikibaseDataFetcher wikibaseDataFetcher;

    @MockBean
    private Queue linkingQueue;

    @MockBean
    private Binding linkingBinding;

    @Test
    public void oneAuthority() throws IOException {
        /*
        when(viafRepository.getResult(any(), eq("PERSON")))
                .thenReturn(TestData.viafPersonResponse());

        LinkedEntityMessage result = linkingOrchestratorService.getLinksForEntity(
                NamedEntityInfo.builder()
                        .authorities(List.of("VIAF"))
                        .entity("Floris Clark")
                        .classification(EntityClassification.PERSON)
                        .build()
        );

        assertEquals(result.getEntity(), "Floris Clark");
        assertEquals(result.getClassification(), EntityClassification.PERSON);

        EntityLinks links = result.getLinks();
        assertNotNull(links);
        assertEquals("Floris Clark", links.getKey());
        assertEquals(1, links.getLinks().size());

        LinkingServiceResult details = links.getLinks().get(0);
        assertEquals(1, details.getMatches().size());
        assertEquals("McLaren, Floris Clark", details.getMatches().get(0).getHeading());
        assertEquals("http://viaf.org/viaf/78272387/", details.getMatches().get(0).getUri());

         */
    }

    @Test
    public void multipleAuthoritiesOrder() throws IOException, MediaWikiApiErrorException {
        /*
        when(viafRepository.getResult(any(), eq("LOCATION")))
                .thenReturn(TestData.viafLocationResponse());

        when(geonamesRepository.getResult(any()))
                .thenReturn(TestData.geonamesResponse());

        when(wikibaseDataFetcher.searchEntities(any(), anyLong()))
                .thenReturn(TestData.wikidataLocationResponse());

        LinkedEntityMessage result = linkingOrchestratorService.getLinksForEntity(
                NamedEntityInfo.builder()
                        .authorities(List.of("VIAF", "GEONAMES", "WIKIDATA"))
                        .entity("Toronto")
                        .classification(EntityClassification.LOCATION)
                        .build()
        );

        assertEquals("Toronto", result.getEntity());
        assertEquals(EntityClassification.LOCATION, result.getClassification());

        EntityLinks links = result.getLinks();
        assertNotNull(links);
        assertEquals(3, links.getLinks().size());

        // The order of authorities given in the request is respected
        assertEquals("VIAF", links.getLinks().get(0).getServiceName());
        assertEquals("Geonames", links.getLinks().get(1).getServiceName());
        assertEquals("Wikidata", links.getLinks().get(2).getServiceName());

         */
    }
}
