package ca.sharcnet.nerve.linking;

import org.springframework.boot.autoconfigure.SpringBootApplication;

// Needed for initializing tests.

@SpringBootApplication
public class TestApplication {
}
