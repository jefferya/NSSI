package ca.sharcnet.nerve.monitor;

public class MonitorModuleConstants {
    public static final String PROFILE_NAME = "monitor";
    public static final String SERVICE_NAME = "Monitor Service";
}
