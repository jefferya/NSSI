package ca.sharcnet.nerve.monitor.service;

import ca.sharcnet.nerve.api.infrastructure.config.NSSIProcessingConfigProperties;
import ca.sharcnet.nerve.broker.message.ProgressMessage;
import ca.sharcnet.nerve.broker.service.RMQUtilsService;
import ca.sharcnet.nerve.job.model.JobDTO;
import ca.sharcnet.nerve.job.service.JobService;
import ca.sharcnet.nerve.job.service.WorkflowETAService;
import ca.sharcnet.nerve.monitor.MonitorModuleConstants;
import ca.sharcnet.nerve.notification.service.NotificationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.AmqpTimeoutException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import static net.logstash.logback.argument.StructuredArguments.kv;

@Profile(MonitorModuleConstants.PROFILE_NAME)
@Service
@Transactional
public class ProgressUpdateService {
    private final JobService jobService;
    private final WorkflowETAService workflowETAService;
    private final NSSIProcessingConfigProperties configProperties;
    private final RMQUtilsService rmqUtils;
    private final NotificationService notifyService;

    private final Logger LOGGER = LoggerFactory.getLogger(ProgressUpdateService.class);

    @Autowired
    ProgressUpdateService(JobService jobService,
                          WorkflowETAService workflowETAService,
                          NSSIProcessingConfigProperties configProperties,
                          RMQUtilsService rmqUtils,
                          NotificationService notifyService)
    {
        this.jobService = jobService;
        this.workflowETAService = workflowETAService;
        this.configProperties = configProperties;
        this.rmqUtils = rmqUtils;
        this.notifyService = notifyService;

        try {
            initializeAverageQueue();
            updateWorkflowETA();
        } catch (Exception e) {
            LOGGER.error("FAILED To Initialize ETA DBs",
                        kv("stackTrace", e.getStackTrace()), 
                        kv("service", MonitorModuleConstants.SERVICE_NAME));
        }
    }

    public void initializeAverageQueue() {
        Map<String, NSSIProcessingConfigProperties.ServiceConfig> services = configProperties.getServices();
        for (String service : services.keySet()) {
            workflowETAService.updateAverageQueue(services.get(service).getJobQueue(), 1.0);
        }
    }

    public void updateJobProgressAndStatus(ProgressMessage message) {
        LOGGER.info("Updating Job Progress and Status",  
                   kv("service", MonitorModuleConstants.SERVICE_NAME),
                   kv("jobId", message.getJobId()),
                   kv("messageSender", message.getServiceName()),
                   kv("requestId", message.getRequestId()));
        jobService.updateProgressCounts(
                message.getJobId(),
                Optional.ofNullable(message.getNumStarted()).orElse(0),
                Optional.ofNullable(message.getNumCompleted()).orElse(0),
                Optional.ofNullable(message.getNumTransientFailures()).orElse(0),
                Optional.ofNullable(message.getNumPermanentFailures()).orElse(0)
        );

        jobService.updateStatus(
                message.getJobId(),
                Optional.ofNullable(message.getCompleted()).orElse(false),
                Optional.ofNullable(message.getFailed()).orElse(false)
        );
    }

    @Scheduled(fixedDelayString = "${nssi.monitor.job.status.check_interval_in_ms}")
    public void checkForCompletedOrFailedJobs() {
        List<JobDTO> completedJobs = jobService.getNewlyCompletedJobs();
        completedJobs.forEach(job -> {
            jobService.updateStatus(job.getId(), true, false);
            LOGGER.info("Job Completed",  
                        kv("service", MonitorModuleConstants.SERVICE_NAME),
                        kv("jobId", job.getId()),
                        kv("requestId", job.getRequestId()));
            notifyService.sendFinishedMessage(job.getId());
        });

        List<JobDTO> failedJobs = jobService.getNewlyFailedJobs();
        failedJobs.forEach(job -> {
            jobService.updateStatus(job.getId(), false, true);
            LOGGER.info("Job Completed With Failures",  
                        kv("service", MonitorModuleConstants.SERVICE_NAME),
                        kv("jobId", job.getId()),
                        kv("requestId", job.getRequestId()));
            notifyService.sendFinishedMessage(job.getId());
        });
    }

    @Scheduled(cron = "${nssi.monitor.job.stalled_cron}")
    public void checkForStalledJobs() {
        List<JobDTO> completedJobs = jobService.getStalledJobs();
        completedJobs.forEach(job -> {
            jobService.updateStatus(job.getId(), true, false);
            LOGGER.info("Job Stalled; Marking as Complete", // FIXME
                    kv("service", MonitorModuleConstants.SERVICE_NAME),
                    kv("jobId", job.getId()),
                    kv("requestId", job.getRequestId()));
        });
    }

    @Scheduled(fixedDelayString = "${nssi.monitor.job.eta_update.check_interval_in_ms}")
    public void updateWorkflowETA() {
        for (String workflow : configProperties.getWorkflows().keySet()) {

            List<NSSIProcessingConfigProperties.ServiceConfig> services = configProperties.getServiceConfigs(workflow);
            Double estimated = services.stream()
                    .map(this::calculateETA)
                    .reduce(0.0, Double::sum);

            workflowETAService.updateETA(workflow, estimated);
        }
    }

    private Double calculateETA(NSSIProcessingConfigProperties.ServiceConfig service) {
        try {
            Integer numInQueue = rmqUtils.numInQueue(service.getJobQueue());
            return numInQueue / workflowETAService.getAverageTime(service.getJobQueue());
        } catch (AmqpTimeoutException e) {
            LOGGER.error("FAILED Connection To RMQ Timed Out",  
                        kv("stackTrace", e.getStackTrace()), 
                        kv("service", MonitorModuleConstants.SERVICE_NAME));
            return 0.0;
        } catch (Exception e) {
            LOGGER.error("FAILED To Calculate ETA",  
                        kv("stackTrace", e.getStackTrace()), 
                        kv("service", MonitorModuleConstants.SERVICE_NAME));
            return 0.0;
        }
    }
}
