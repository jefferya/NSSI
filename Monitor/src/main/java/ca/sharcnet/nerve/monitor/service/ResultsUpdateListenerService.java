package ca.sharcnet.nerve.monitor.service;

import ca.sharcnet.nerve.broker.infrastructure.config.RMQConfiguration;
import ca.sharcnet.nerve.broker.message.ResultMessage;
import ca.sharcnet.nerve.monitor.MonitorModuleConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

import static net.logstash.logback.argument.StructuredArguments.kv;

@Profile(MonitorModuleConstants.PROFILE_NAME)
@Service
public class ResultsUpdateListenerService {

    private ResultsUpdateService resultsUpdateService;

    private final Logger LOGGER = LoggerFactory.getLogger(ProgressUpdateService.class);

    @Autowired
    ResultsUpdateListenerService(ResultsUpdateService resultsUpdateService) {
        this.resultsUpdateService = resultsUpdateService;
    }

    @RabbitListener(queues = RMQConfiguration.RESULT_QUEUE)
    public void listen(ResultMessage message) {
        LOGGER.info("Received Results Message",
                   kv("service", MonitorModuleConstants.SERVICE_NAME),
                   kv("jobId", message.getJobId()),
                   kv("requestId", message.getRequestId()));
        resultsUpdateService.updateJobResultsInfo(message);
        try {
            TimeUnit.MILLISECONDS.sleep(250); // FIXME
        } catch (InterruptedException ie) {
            Thread.currentThread().interrupt();
        }
    }
}
