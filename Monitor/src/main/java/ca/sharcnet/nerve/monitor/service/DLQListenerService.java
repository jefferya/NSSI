package ca.sharcnet.nerve.monitor.service;

import ca.sharcnet.nerve.broker.infrastructure.config.RMQConfiguration;
import ca.sharcnet.nerve.broker.message.ServiceMessage;
import ca.sharcnet.nerve.broker.service.RMQUtilsService;
import ca.sharcnet.nerve.monitor.MonitorModuleConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import static net.logstash.logback.argument.StructuredArguments.kv;

@Profile(MonitorModuleConstants.PROFILE_NAME)
@Service
public class DLQListenerService {

    private RabbitTemplate rabbitTemplate;
    private RMQUtilsService rmqUtils;

    private final Logger LOGGER = LoggerFactory.getLogger(DLQListenerService.class);

    @Autowired
    DLQListenerService(RabbitTemplate rabbitTemplate,
                       RMQUtilsService rmqUtils) {
        this.rabbitTemplate = rabbitTemplate;
        this.rmqUtils = rmqUtils;
    }

    @RabbitListener(queues = RMQConfiguration.DLQ)
    public void listen(Message message) {
        MessageProperties props = message.getMessageProperties();
        String originalExchange = props.getHeader("x-original-exchange");
        String originalRoutingKey = props.getHeader("x-original-routing-key");

        LOGGER.info("Retrying Message Off DLQ",  
                   kv("service", MonitorModuleConstants.SERVICE_NAME),
                   kv("jobId", rmqUtils.getJobIdFromMessage(message, ServiceMessage.class).orElse(-1L)),
                   kv("exchange", originalExchange),
                   kv("routingKey", originalRoutingKey),
                   kv("requestId", rmqUtils.getRequestIdFromMessage(message, ServiceMessage.class).orElse("-1")));

        rabbitTemplate.convertAndSend(originalExchange, originalRoutingKey, message);
    }
}
