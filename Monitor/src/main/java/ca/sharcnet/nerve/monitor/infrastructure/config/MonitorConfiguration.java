package ca.sharcnet.nerve.monitor.infrastructure.config;

import ca.sharcnet.nerve.monitor.MonitorModuleConstants;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Profile(MonitorModuleConstants.PROFILE_NAME)
@Configuration
public class MonitorConfiguration {
}
