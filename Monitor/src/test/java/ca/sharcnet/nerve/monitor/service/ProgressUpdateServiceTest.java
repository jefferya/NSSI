package ca.sharcnet.nerve.monitor.service;

import ca.sharcnet.nerve.api.infrastructure.config.NSSIProcessingConfigProperties;
import ca.sharcnet.nerve.broker.message.ProgressMessage;
import ca.sharcnet.nerve.broker.service.RMQUtilsService;
import ca.sharcnet.nerve.elucidate_api.service.AnnotationClient;
import ca.sharcnet.nerve.job.repository.Job;
import ca.sharcnet.nerve.job.repository.JobRepository;
import ca.sharcnet.nerve.job.service.JobService;
import ca.sharcnet.nerve.job.service.WorkflowETAService;
import ca.sharcnet.nerve.notification.service.NotificationService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@ActiveProfiles("monitor")
@SpringBootTest
public class ProgressUpdateServiceTest {

    @MockBean
    private JobRepository jobRepository;

    @MockBean
    private ProgressUpdateListenerService progressUpdateListenerService;

    @MockBean
    private ResultsUpdateListenerService resultsUpdateListenerService;

    @MockBean
    private AnnotationClient annotationClient;

    @MockBean
    private NSSIProcessingConfigProperties configProperties;

    @MockBean
    private WorkflowETAService workflowETAService;

    @MockBean
    private RMQUtilsService rmqUtils;

    @MockBean
    private NotificationService notifyService;

    @Autowired
    private JobService jobService;

    @Autowired
    private ProgressUpdateService progressUpdateService;

    @Test
    public void updateProgress_tallies() {
        Job job = basicJob(1L);
        when(jobRepository.findById(eq(1L)))
                .thenReturn(Optional.of(job));

        ProgressMessage message = ProgressMessage.builder().jobId(1L).numStarted(10).numCompleted(5).build();
        progressUpdateService.updateJobProgressAndStatus(message);

        verify(jobRepository, times(1)).incrementCounts(
                eq(1L), eq(10), eq(5), eq(0), eq(0)
        );
        verify(jobRepository, times(0)).markCompleted(any());
        verify(jobRepository, times(0)).markFailed(any());
    }

    @Test
    public void updateProgress_statuses() {
        Job completedJob = basicJob(1L);
        when(jobRepository.findById(eq(1L)))
                .thenReturn(Optional.of(completedJob));

        ProgressMessage completedMessage = ProgressMessage.builder().jobId(1L).completed(true).build();
        progressUpdateService.updateJobProgressAndStatus(completedMessage);

        verify(jobRepository, times(1)).incrementCounts(
                eq(1L), eq(0), eq(0), eq(0), eq(0)
        );
        verify(jobRepository, times(1)).markCompleted(eq(1L));
        verify(jobRepository, times(0)).markCancelled(eq(1L));
        verify(jobRepository, times(0)).markFailed(eq(1L));

        Job failedJob = basicJob(3L);
        when(jobRepository.findById(eq(3L)))
                .thenReturn(Optional.of(failedJob));

        ProgressMessage failedMessage = ProgressMessage.builder().jobId(3L).failed(true).build();
        progressUpdateService.updateJobProgressAndStatus(failedMessage);

        verify(jobRepository, times(1)).incrementCounts(
                eq(3L), eq(0), eq(0), eq(0), eq(0)
        );
        verify(jobRepository, times(0)).markCompleted(eq(3L));
        verify(jobRepository, times(1)).markFailed(eq(3L));
    }

    @Test
    public void updateProgress_ignoreDeadJobs() {
        Job cancelledJob = basicJob(1L);
        cancelledJob.setCancelled(true);

        Job failedJob = basicJob(2L);
        failedJob.setFailed(true);

        when(jobRepository.findById(eq(1L))).thenReturn(Optional.of(cancelledJob));
        when(jobRepository.findById(eq(2L))).thenReturn(Optional.of(failedJob));

        ProgressMessage cancelledMessage = ProgressMessage.builder().jobId(1L).numStarted(1).numCompleted(0).build();
        progressUpdateService.updateJobProgressAndStatus(cancelledMessage);

        verify(jobRepository, times(0)).incrementCounts(
                eq(1L), any(), any(), any(), any()
        );
        verify(jobRepository, times(0)).markCompleted(eq(1L));
        verify(jobRepository, times(0)).markFailed(eq(1L));

        ProgressMessage failedMessage = ProgressMessage.builder().jobId(1L).numStarted(1).numCompleted(0).build();
        progressUpdateService.updateJobProgressAndStatus(failedMessage);

        verify(jobRepository, times(0)).incrementCounts(
                eq(2L), any(), any(), any(), any()
        );
        verify(jobRepository, times(0)).markCompleted(eq(2L));
        verify(jobRepository, times(0)).markFailed(eq(2L));
    }

    @Test
    public void updateProgress_statusAndCounts() {
        Job job = basicJob(1L);
        when(jobRepository.findById(eq(1L)))
                .thenReturn(Optional.of(job));

        ProgressMessage message = ProgressMessage.builder()
                .jobId(1L).numStarted(5).numCompleted(5).completed(true)
                .build();
        progressUpdateService.updateJobProgressAndStatus(message);

        verify(jobRepository, times(1)).incrementCounts(
                eq(1L), eq(5), eq(5), eq(0), eq(0)
        );
        verify(jobRepository, times(1)).markCompleted(eq(1L));
        verify(jobRepository, times(0)).markFailed(any());
    }

    @Test
    public void workflowETA() {
        Map<String, NSSIProcessingConfigProperties.WorkflowConfig> test = new HashMap<>();
        test.put("Test1", null);
        test.put("Test2", null);
        when(configProperties.getWorkflows()).thenReturn(test);

        List<NSSIProcessingConfigProperties.ServiceConfig> services1 = createTestService(1);
        when(configProperties.getServiceConfigs("Test1")).thenReturn(services1);
        when(rmqUtils.numInQueue(services1.get(0).getJobQueue())).thenReturn(5);
        when(rmqUtils.numInQueue(services1.get(1).getJobQueue())).thenReturn(5);
        when(workflowETAService.getAverageTime(services1.get(0).getJobQueue())).thenReturn(Double.valueOf(1));
        when(workflowETAService.getAverageTime(services1.get(1).getJobQueue())).thenReturn(Double.valueOf(1));

        List<NSSIProcessingConfigProperties.ServiceConfig> services2 = createTestService(3);
        when(configProperties.getServiceConfigs("Test2")).thenReturn(services2);
        when(rmqUtils.numInQueue(services2.get(0).getJobQueue())).thenReturn(10);
        when(rmqUtils.numInQueue(services2.get(1).getJobQueue())).thenReturn(10);
        when(workflowETAService.getAverageTime(services2.get(0).getJobQueue())).thenReturn(Double.valueOf(1));
        when(workflowETAService.getAverageTime(services2.get(1).getJobQueue())).thenReturn(Double.valueOf(1));

        progressUpdateService.updateWorkflowETA();

        ArgumentCaptor<String> strCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Double> dblCaptor = ArgumentCaptor.forClass(Double.class);
        verify(workflowETAService, times(2)).updateETA(strCaptor.capture(), dblCaptor.capture());
        List<String> args1 = strCaptor.getAllValues();
        List<Double> args2 = dblCaptor.getAllValues();
        
        assertEquals("Test1", args1.get(0));
        assertEquals(Double.valueOf(10), args2.get(0));   
        assertEquals("Test2", args1.get(1));
        assertEquals(Double.valueOf(20), args2.get(1));
    }

    private List<NSSIProcessingConfigProperties.ServiceConfig> createTestService(int i) {
        List<NSSIProcessingConfigProperties.ServiceConfig> services = new ArrayList<>();
        NSSIProcessingConfigProperties.ServiceConfig config1 = new NSSIProcessingConfigProperties.ServiceConfig();
        config1.setJobQueue("Queue " + i);
        services.add(config1);
        NSSIProcessingConfigProperties.ServiceConfig config2 = new NSSIProcessingConfigProperties.ServiceConfig();
        config2.setJobQueue("Queue " + (i+1));
        services.add(config2);

        return services;
    }

    private Job basicJob(Long id) {
        return Job.builder()
                .id(id)
                .createdBy("test")
                .startTime(LocalDateTime.now())
                .lastUpdatedTime(LocalDateTime.now())
                .numTotal(0)
                .numCompleted(0)
                .allCompleted(false)
                .cancelled(false)
                .failed(false)
                .build();
    }
}
