package ca.sharcnet.nerve.elucidate_api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TextPositionSelector {

    @JsonProperty("@type")
    private String type;

    @JsonProperty("oa:start")
    private Long start;

    @JsonProperty("oa:end")
    private Long end;
}
