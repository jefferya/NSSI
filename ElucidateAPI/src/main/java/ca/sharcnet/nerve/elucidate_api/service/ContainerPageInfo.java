package ca.sharcnet.nerve.elucidate_api.service;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ContainerPageInfo {
    private String type;

    @JsonProperty("as:items")
    private Object items; // TODO

    private String partOf;
    private Integer startIndex;
}
