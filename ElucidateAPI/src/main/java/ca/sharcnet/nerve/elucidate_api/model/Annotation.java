package ca.sharcnet.nerve.elucidate_api.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Annotation {

    @JsonProperty("@context")
    private AnnotationContext jsonContext;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("@id")
    private String id;

    @JsonProperty("@type")
    private String type;

    @JsonProperty("dcterms:creator")
    private AnnotationCreator creator;

    @JsonProperty("dcterms:created")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime created;

    @JsonProperty("dcterms:issued")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime issued;

    @JsonProperty("oa:motivatedBy")
    private String motivatedBy;

    @JsonProperty("oa:hasTarget")
    private AnnotationTarget target;

    @JsonProperty("oa:hasBody")
    private AnnotationChoice body;
}
