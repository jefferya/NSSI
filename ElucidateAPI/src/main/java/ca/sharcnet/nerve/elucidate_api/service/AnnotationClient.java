package ca.sharcnet.nerve.elucidate_api.service;

import ca.sharcnet.nerve.elucidate_api.ElucidateAPIModuleConstants;
import ca.sharcnet.nerve.elucidate_api.model.Annotation;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Objects;

import static net.logstash.logback.argument.StructuredArguments.kv;

@Service
public class AnnotationClient {
    private final String serverURI;
    private RestTemplate restTemplate;
    private ObjectMapper mapper;

    private final String ANNOTATION_CONTEXT = "http://www.w3.org/ns/anno.jsonld";
    private final String LD_PLATFORM_CONTEXT = "http://www.w3.org/ns/ldp.jsonld";

    private final String W3C_CONTAINER_CREATE = "/w3c/";

    private final String MEDIATYPE_LD_JSON = "application/ld+json";

    private final Logger LOGGER = LoggerFactory.getLogger(AnnotationClient.class);

    @Autowired
    public AnnotationClient(@Value("${annotation-server.base_url}") String serverURI,
                            RestTemplate restTemplate,
                            ObjectMapper mapper)
    {
        this.serverURI = serverURI;
        this.restTemplate = restTemplate;
        this.mapper = mapper;
    }

    public String createNewContainer(String documentUri, String requestId)
    {
        ContainerCreateRequest request = ContainerCreateRequest.builder()
                .jsonContext(Arrays.asList(
                        ANNOTATION_CONTEXT,
                        LD_PLATFORM_CONTEXT))
                .containerType(Arrays.asList(
                        ContainerType.BASIC_CONTAINER,
                        ContainerType.ANNOTATION_COLLECTION))
                .label(documentUri)
                .build();

        try {
            String requestJson = mapper.writeValueAsString(request);

            HttpHeaders headers = new HttpHeaders();
            headers.add("Accept", MEDIATYPE_LD_JSON);
            headers.add("Content-Type", MEDIATYPE_LD_JSON);


            HttpEntity<String> httpRequest = new HttpEntity<>(requestJson, headers);
            String requestUri = serverURI + W3C_CONTAINER_CREATE;

            LOGGER.info("Creating New Annotation Container",  
                       kv("service", ElucidateAPIModuleConstants.SERVICE_NAME),
                       kv("documentUri", documentUri),
                       kv("requestUri", requestUri),
                       kv("requestJson", requestJson),
                       kv("requestId", requestId));

            ContainerResponse response = restTemplate.postForObject(
                    requestUri,
                    httpRequest,
                    ContainerResponse.class
            );

            return response.getId();
        } catch (JsonProcessingException | RestClientException e) {
            LOGGER.error("FAILED To Create New Annotation Container",  
                        kv("stackTrace", e.getStackTrace()), 
                        kv("service", ElucidateAPIModuleConstants.SERVICE_NAME),
                        kv("documentUri", documentUri),
                        kv("requestId", requestId));
            return null; // FIXME FIXME FIXME
        }
    }

    public void addAnnotation(String containerURI,
                              Annotation annotation,
                              String requestId)
    {
        String requestJson = null;
        try {
            requestJson = mapper.writeValueAsString(annotation);
        } catch (JsonProcessingException e) {
            LOGGER.error("FAILED To Add Annotation",  
                        kv("stackTrace", e.getStackTrace()), 
                        kv("service", ElucidateAPIModuleConstants.SERVICE_NAME),
                        kv("container", containerURI),
                        kv("requestId", requestId));
        }

        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", MEDIATYPE_LD_JSON);
        headers.add("Content-Type", MEDIATYPE_LD_JSON);

        HttpEntity<String> httpRequest = new HttpEntity<>(requestJson, headers);

        try {
            AnnotationResponse annotationResponse = restTemplate.postForObject(containerURI, httpRequest, AnnotationResponse.class);
            if (Objects.nonNull(annotationResponse) && Objects.nonNull(annotationResponse.getId())) {
                LOGGER.info("Adding New Annotation",  
                           kv("service", ElucidateAPIModuleConstants.SERVICE_NAME),
                           kv("container", containerURI),
                           kv("annotation", annotationResponse.getId()),
                           kv("requestId", requestId));
            }
        } catch (Exception e) {
            LOGGER.error("FAILED To Add Annotation",  
                        kv("stackTrace", e.getStackTrace()), 
                        kv("service", ElucidateAPIModuleConstants.SERVICE_NAME),
                        kv("container", containerURI),
                        kv("requestId", requestId));
        }
    }
}
