package ca.sharcnet.nerve.elucidate_api.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AnnotationBody {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("@type")
    private String type;

    @JsonProperty("@id")
    private String id;

    @JsonProperty("dc:format")
    private String format;

    @JsonProperty("rdfs:label")
    private String label;

    @JsonProperty("rdfs:comment")
    private String description;
}
