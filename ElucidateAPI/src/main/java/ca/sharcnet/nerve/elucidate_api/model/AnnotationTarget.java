package ca.sharcnet.nerve.elucidate_api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AnnotationTarget {

    @JsonProperty("@type")
    private String type;

    @JsonProperty("oa:hasSource")
    private String source;

    @JsonProperty("dc:format")
    private String format;

    @JsonProperty("oa:hasSelector")
    private RefinableTextQuoteSelector selector;
}
