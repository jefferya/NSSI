package ca.sharcnet.nerve.elucidate_api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RefinableTextQuoteSelector {

    @JsonProperty("@type")
    private String type;

    @JsonProperty("oa:exact")
    private String exact;

    @JsonProperty("oa:refinedBy")
    private TextPositionSelector refinedBy;
}
