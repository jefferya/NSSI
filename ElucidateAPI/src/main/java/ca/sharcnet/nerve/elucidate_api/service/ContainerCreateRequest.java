package ca.sharcnet.nerve.elucidate_api.service;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ContainerCreateRequest {

    @JsonProperty("@context")
    private List<String> jsonContext;

    @JsonProperty("type")
    private List<ContainerType> containerType;

    private String label;
}
