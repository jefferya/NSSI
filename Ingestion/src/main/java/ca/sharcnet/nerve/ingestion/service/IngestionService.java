package ca.sharcnet.nerve.ingestion.service;

import ca.sharcnet.nerve.broker.message.NSSIServiceMessage;
import ca.sharcnet.nerve.broker.message.ProgressMessage;
import ca.sharcnet.nerve.broker.service.RMQUtilsService;
import ca.sharcnet.nerve.ingestion.IngestionModuleConstants;
import ca.sharcnet.nerve.job.service.WorkflowETAService;
import ca.sharcnet.nerve.s3_api.service.LincsS3Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Profile(IngestionModuleConstants.PROFILE_NAME)
@Service
public class IngestionService {
    private final BlazegraphClient bgClient;
    private final LincsS3Service lincsS3Service;
    private final RMQUtilsService rmqUtils;
    private final WorkflowETAService etaService;
    private int numMessages = 0;

    private final Logger LOGGER = LoggerFactory.getLogger(IngestionService.class);

    @Autowired
    public IngestionService(BlazegraphClient bgClient,
                            LincsS3Service lincsS3Service,
                            RMQUtilsService rmqUtils,
                            WorkflowETAService etaService)
    {
        this.bgClient = bgClient;
        this.lincsS3Service = lincsS3Service;
        this.rmqUtils = rmqUtils;
        this.etaService = etaService;
    }

    @RabbitListener(queues = IngestionModuleConstants.QUEUE_NAME)
    public void listen(Message message) {
        numMessages = etaService.checkUpdateAverage(rmqUtils.numInQueue(IngestionModuleConstants.QUEUE_NAME), 
                                                    numMessages, 
                                                    IngestionModuleConstants.QUEUE_NAME);
        rmqUtils.processWithRetry(message, NSSIServiceMessage.class, (startMessage -> {
            Map<String, String> context = startMessage.getContext();

            if (context.containsKey("S3_OBJECT_NAME")) {
                bgClient.transferFile(context.get("S3_OBJECT_NAME"), 
                                    context.get("S3_BUCKET"), 
                                    context.get("POST_URL"), 
                                    context.getOrDefault("S3_URL", IngestionModuleConstants.S3_URL),
                                    startMessage.getRequestId());
            } else if (context.containsKey("S3_OBJECT_PREFIX")) {
                getFilesFromDir(context.get("S3_OBJECT_PREFIX"), 
                                context.get("S3_BUCKET"), 
                                context.get("POST_URL"), 
                                context.getOrDefault("S3_URL", IngestionModuleConstants.S3_URL),
                                startMessage.getRequestId());
            }

            ProgressMessage progressMessage = ProgressMessage.builder()
                    .jobId(startMessage.getJobId())
                    .requestId(startMessage.getRequestId())
                    .completed(true)
                    .build();
            rmqUtils.sendProgressMessage(progressMessage);
        }));
    }

    private void getFilesFromDir(String objectPrefix, String bucket, String postUrl, String s3Url, String requestId) {
        List<String> files = lincsS3Service.getAllObjectNames(bucket, objectPrefix, requestId);
        for (String name : files) {
            bgClient.transferFile(name, bucket, postUrl, s3Url, requestId);
        }
    }

}