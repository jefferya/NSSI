package ca.sharcnet.nerve.ingestion.service;

import ca.sharcnet.nerve.ingestion.IngestionModuleConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import static net.logstash.logback.argument.StructuredArguments.kv;

@Profile(IngestionModuleConstants.PROFILE_NAME)
@Service
public class BlazegraphClient {
    private final Logger LOGGER = LoggerFactory.getLogger(BlazegraphClient.class);
    private RestTemplate restTemplate;
    private String user;
    private String pass;

    @Autowired
    public BlazegraphClient(RestTemplate restTemplate,
                            @Value("${nssi.rs.username}") String user,
                            @Value("${nssi.rs.password}") String pass) {
        this.restTemplate = restTemplate;
        this.user = user;
        this.pass = pass;
    }

    public void transferFile(String objectName, String bucket, String postUrl, String s3Url, String requestId) {
        String[] objectNameSplit = objectName.split("[.]", 0);
        String cleanedObjectName = objectNameSplit[0];

        String[] pathSplit = cleanedObjectName.split("/");
        cleanedObjectName = pathSplit[pathSplit.length - 2] + "/" + pathSplit[pathSplit.length - 1];

        sendUploadReq(objectName, cleanedObjectName, bucket, postUrl, s3Url, requestId);
    }

    private void sendUploadReq(String objectName, String cleanedObjectName, String bucket, String postUrl, String s3Url, String requestId) {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            headers.setBasicAuth(user, pass);

            MultiValueMap<String, String> map= new LinkedMultiValueMap<>();
            map.add("update", 
            "LOAD <" + s3Url + "/" + bucket + "/" + objectName + ">"
            + " INTO GRAPH <" + IngestionModuleConstants.GRAPH_URL + "/" + cleanedObjectName + ">");

            HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
            ResponseEntity<String> response = restTemplate.postForEntity(postUrl, request, String.class);
            LOGGER.info("Sending Upload Request",  
                       kv("service", IngestionModuleConstants.SERVICE_NAME),
                       kv("objectName", cleanedObjectName),
                       kv("bucket", bucket),
                       kv("postUrl", postUrl),
                       kv("s3Url", s3Url),
                       kv("response", response.getStatusCode()),
                       kv("requestId", requestId));
        } catch (RestClientException e) {
            LOGGER.error("FAILED To Post Upload Request",  
                        kv("stackTrace", e.getStackTrace()), 
                        kv("service", IngestionModuleConstants.SERVICE_NAME),
                        kv("objectName", cleanedObjectName),
                        kv("bucket", bucket),
                        kv("postUrl", postUrl),
                        kv("s3Url", s3Url),
                        kv("requestId", requestId));
        }
    }

}