package ca.sharcnet.nerve.ingestion;

public class IngestionModuleConstants {
    public static final String PROFILE_NAME = "ingestion";
    public static final String QUEUE_NAME = "ingestionQueue";
    public static final String S3_URL = "https://backup.lincsproject.ca";
    public static final String GRAPH_URL = "http://graph.lincsproject.ca";
    public static final String SERVICE_NAME = "Ingestion Service";
}
