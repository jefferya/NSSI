package ca.sharcnet.nerve.ingestion.infastructure.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

@Component
public class UrlEncodingConfig extends FormHttpMessageConverter {
    private final Logger LOGGER = LoggerFactory.getLogger(UrlEncodingConfig.class);

    @Override
    protected String serializeForm(MultiValueMap<String, Object> formData, Charset charset) {
        return super.serializeForm(formData, StandardCharsets.UTF_8);
    }

}