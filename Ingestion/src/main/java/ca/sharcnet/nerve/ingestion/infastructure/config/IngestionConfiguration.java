package ca.sharcnet.nerve.ingestion.infastructure.config;

import ca.sharcnet.nerve.ingestion.IngestionModuleConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import static ca.sharcnet.nerve.broker.infrastructure.config.RMQConfiguration.*;
import static ca.sharcnet.nerve.ingestion.IngestionModuleConstants.QUEUE_NAME;

@Profile(IngestionModuleConstants.PROFILE_NAME)
@Configuration
public class IngestionConfiguration {
    private final Logger LOGGER = LoggerFactory.getLogger(IngestionConfiguration.class);

    @Bean
    public Queue ingestionQueue() {
        return QueueBuilder.durable(QUEUE_NAME)
                .withArgument(DLQ_KEY, DLX_APP_MESSAGES)
                .build();
    }

    @Bean
    public Binding ingestionBinding(@Qualifier(APP_MESSAGES_EXCHANGE) DirectExchange exchange) {
        return BindingBuilder.bind(ingestionQueue()).to(exchange)
                .with(QUEUE_NAME);
    }
}