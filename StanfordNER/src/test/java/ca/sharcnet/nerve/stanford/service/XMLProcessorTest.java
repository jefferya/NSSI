package ca.sharcnet.nerve.stanford.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ActiveProfiles("stanford")
@SpringBootTest
public class XMLProcessorTest {

    @Test
    public void resultMessage() 
    {
        XMLProcessorUtils processor = new XMLProcessorUtils(new ObjectMapper());

        Assert.assertTrue(processor.cleanLemma("testtest12").equals("testtest12"));
        Assert.assertTrue(processor.cleanLemma("\"test\"   1").equals("test 1"));
        Assert.assertTrue(processor.cleanLemma("\"test     123\"\"\"").equals("test 123"));
    }

}