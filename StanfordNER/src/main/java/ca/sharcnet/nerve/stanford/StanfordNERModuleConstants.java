package ca.sharcnet.nerve.stanford;

public class StanfordNERModuleConstants {
    public static final String PROFILE_NAME = "stanfordner";
    public static final String QUEUE_NAME = "stanfordnerQueue";
    public static final String SERVICE_NAME = "Stanford NER Service";
}
