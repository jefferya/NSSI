package ca.sharcnet.nerve.stanford.service;

import ca.sharcnet.nerve.broker.message.NSSIServiceMessage;
import ca.sharcnet.nerve.broker.message.ProgressMessage;
import ca.sharcnet.nerve.broker.service.RMQUtilsService;
import ca.sharcnet.nerve.stanford.StanfordNERModuleConstants;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import static net.logstash.logback.argument.StructuredArguments.kv;

@Profile(StanfordNERModuleConstants.PROFILE_NAME)
@Service
public class PlaintextProcessor {
    private final XMLProcessorUtils xmlProcessorUtils;
    private final RMQUtilsService rmqUtils;
    private final ObjectMapper objectMapper;

    private final Logger LOGGER = LoggerFactory.getLogger(PlaintextProcessor.class);

    @Autowired
    PlaintextProcessor(XMLProcessorUtils xmlProcessorUtils,
                       RMQUtilsService rmqUtils,
                       ObjectMapper objectMapper)
    {
        this.xmlProcessorUtils = xmlProcessorUtils;
        this.rmqUtils = rmqUtils;
        this.objectMapper = objectMapper;
    }

    public void handleMessage(NSSIServiceMessage message,
                              NERPipeline classifier)
    {
        var classifierResults = classifier.run(message.getDocument(), 0);
        if (classifierResults.size() == 0) {
            LOGGER.info("No entities found.",
                    kv("service", StanfordNERModuleConstants.SERVICE_NAME),
                    kv("jobId", message.getJobId()),
                    kv("requestId", message.getRequestId()));

            ProgressMessage progressMessage = ProgressMessage.builder()
                    .jobId(message.getJobId())
                    .requestId(message.getRequestId())
                    .serviceName(StanfordNERService.class.getName())
                    .completed(true)
                    .build();

            rmqUtils.sendProgressMessage(progressMessage);
        } else {
            var namedEntities = xmlProcessorUtils.getNamedEntityInfo(classifierResults);
            var resultMessages = xmlProcessorUtils.convertNamedEntityResults(namedEntities, message);

            resultMessages.forEach(rmqUtils::sendToNextStep);

            LOGGER.info("Sending Progress Message",
                    kv("service", StanfordNERModuleConstants.SERVICE_NAME),
                    kv("jobId", message.getJobId()),
                    kv("size", classifierResults.size()),
                    kv("requestId", message.getRequestId()));

            ProgressMessage progressMessage = ProgressMessage.builder()
                    .jobId(message.getJobId())
                    .requestId(message.getRequestId())
                    .serviceName(StanfordNERService.class.getName())
                    .numStarted(classifierResults.size())
                    .build();

            rmqUtils.sendProgressMessage(progressMessage);
        }
    }
}
