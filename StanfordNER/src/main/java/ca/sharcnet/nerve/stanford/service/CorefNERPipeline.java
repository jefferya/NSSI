package ca.sharcnet.nerve.stanford.service;


import edu.stanford.nlp.coref.CorefCoreAnnotations;
import edu.stanford.nlp.pipeline.CoreDocument;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.util.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Properties;
import java.util.stream.Collectors;

public class CorefNERPipeline implements NERPipeline {
    private Properties props;
    private StanfordCoreNLP pipeline;
    private final XMLProcessorUtils xmlUtils;

    private final static Logger LOGGER = LoggerFactory.getLogger(CorefNERPipeline.class);

    public CorefNERPipeline(XMLProcessorUtils xmlUtils)
            throws ClassCastException
    {
        this.xmlUtils = xmlUtils;

        LOGGER.info("initializing coref pipeline");

        props = new Properties();
        props.setProperty("annotators", "tokenize,ssplit,pos,lemma,ner,parse,depparse,coref");
        props.setProperty("ner.applyFineGrained", "false");
        props.setProperty("ner.applyNumericClassifiers", "false");

        pipeline = new StanfordCoreNLP(props);

        LOGGER.info("coref pipeline created");
    }

    public List<CorrelatedNERResults> run(String output, Integer offset) {
        CoreDocument doc = new CoreDocument(output);
        pipeline.annotate(doc);

        var entityMentions = doc.annotation().get(
                CorefCoreAnnotations.CorefMentionsAnnotation.class
        ).stream()
                .filter(mention -> Objects.nonNull(mention.nerName()))
                .collect(
                        Collectors.toMap(
                                mention -> mention.mentionID,
                                mention -> {
                                    var originalSpan = mention.originalSpan;
                                    return MentionResult.builder()
                                            .type(mention.nerString)
                                            .value(mention.nerName())
                                            .startPosition(originalSpan.get(0).beginPosition() + offset)
                                            .endPosition(originalSpan.get(originalSpan.size() - 1).endPosition() + offset)
                                            .build();
                                }
                        )
                );

        List<CorrelatedNERResults> result = new ArrayList<>();

        doc.corefChains().values().forEach(cc -> {
            var representative = cc.getRepresentativeMention();
            if (entityMentions.containsKey(representative.mentionID)) {
                var ems = cc.getMentionsInTextualOrder().stream()
                        .map(mention -> entityMentions.get(mention.mentionID))
                        .filter(Objects::nonNull)
                        .collect(Collectors.toList());

                result.add(
                        CorrelatedNERResults.builder()
                                .representative(xmlUtils.cleanLemma(representative.mentionSpan))
                                .entityType(representative.mentionType.name())
                                .allMatches(
                                        ems.stream().map(mentionResult -> Pair.of(
                                                mentionResult.getValue(),
                                                Pair.of(
                                                        mentionResult.getStartPosition(),
                                                        mentionResult.getEndPosition()
                                                )
                                        )).collect(Collectors.toList())
                                )
                                .build()
                );

                cc.getMentionsInTextualOrder().forEach(mention -> entityMentions.remove(mention.mentionID));
            }
        });

        entityMentions.values().forEach(mention ->
                result.add(
                        CorrelatedNERResults.builder()
                                .representative(xmlUtils.cleanLemma(mention.getValue()))
                                .entityType(mention.getType())
                                .allMatch(Pair.of(
                                        mention.getValue(),
                                        Pair.of(
                                                mention.getStartPosition(),
                                                mention.getEndPosition()
                                        )
                                ))
                                .build()
                )
        );

        return result;
    }
}

