package ca.sharcnet.nerve.stanford.service;

import edu.stanford.nlp.util.Interval;
import edu.stanford.nlp.util.IntervalTree;
import lombok.*;
import org.springframework.data.util.Pair;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CorrelatedNERResults {
    /**
     * The most representative string for all of the entity mentions encompassed
     * in this set of correlated entities.
     */
    private String representative;

    /**
     * The entity type that applies to all entities within this set of correlated
     * entities.
     */
    private String entityType;

    /**
     * A list of correlated entities in the form of (entity mention, (start position,
     * end position)) pairs.
     */
    @Singular
    private List<Pair<String, Pair<Integer, Integer>>> allMatches;

    /**
     * Returns all matches that occur after the given {@code endPosition}.
     *
     * @param endPosition the position after which to look for entities
     */
    public Optional<CorrelatedNERResults> filterMatchesByEndPosition(int endPosition) {
        allMatches = allMatches.stream().filter(m -> m.getSecond().getSecond() > endPosition)
                .collect(Collectors.toList());

        if (allMatches.isEmpty()) {
            return Optional.empty();
        }

        return Optional.of(this);
    }

    /**
     * Returns all matches that do not overlap with the start and end positions
     * given in {@code intervalsToIgnore}
     * @param intervalsToIgnore a collection of intervals which demarcate sections
     *                          of the text to ignore.
     */
    public Optional<CorrelatedNERResults> filterMatchesByOverlap(
            IntervalTree<Integer, Interval<Integer>> intervalsToIgnore
    )
    {
        allMatches = allMatches.stream().filter(m -> !intervalsToIgnore.overlaps(
                Interval.toInterval(m.getSecond().getFirst(), m.getSecond().getSecond()))
        ).collect(Collectors.toList());

        if (allMatches.isEmpty()) {
            return Optional.empty();
        }

        return Optional.of(this);
    }
}
