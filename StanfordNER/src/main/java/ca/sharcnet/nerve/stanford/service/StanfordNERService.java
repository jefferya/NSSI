package ca.sharcnet.nerve.stanford.service;

import ca.sharcnet.nerve.broker.message.NSSIServiceMessage;
import ca.sharcnet.nerve.broker.service.RMQUtilsService;
import ca.sharcnet.nerve.job.service.WorkflowETAService;
import ca.sharcnet.nerve.stanford.StanfordNERModuleConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import static net.logstash.logback.argument.StructuredArguments.kv;

@Profile(StanfordNERModuleConstants.PROFILE_NAME)
@Service
public class StanfordNERService {
    private final XMLProcessor xmlProcessor;
    private final PlaintextProcessor plaintextProcessor;
    private final DefaultNERPipeline defaultPipeline;
    private final CorefNERPipeline corefPipeline;
    private final RMQUtilsService rmqUtils;
    private WorkflowETAService etaService;
    private int numMessages = 0;

    private final Logger LOGGER = LoggerFactory.getLogger(StanfordNERService.class);

    @Autowired
    public StanfordNERService(DefaultNERPipeline defaultPipeline,
                              CorefNERPipeline corefPipeline,
                              XMLProcessor xmlProcessor,
                              PlaintextProcessor plaintextProcessor,
                              RMQUtilsService rmqUtils,
                              WorkflowETAService etaService)
    {
        this.defaultPipeline = defaultPipeline;
        this.corefPipeline = corefPipeline;
        this.xmlProcessor = xmlProcessor;
        this.plaintextProcessor = plaintextProcessor;
        this.rmqUtils = rmqUtils;
        this.etaService = etaService;
    }

    @RabbitListener(queues = StanfordNERModuleConstants.QUEUE_NAME)
    public void listen(Message message) {
        numMessages = etaService.checkUpdateAverage(rmqUtils.numInQueue(StanfordNERModuleConstants.QUEUE_NAME),
                numMessages,
                StanfordNERModuleConstants.QUEUE_NAME);
        rmqUtils.processWithRetry(message, NSSIServiceMessage.class, (extractedTextMessage -> {
                    LOGGER.info("Processing Stanford NER Message",
                            kv("service", StanfordNERModuleConstants.SERVICE_NAME),
                            kv("documentUri", extractedTextMessage.getDocumentURI()),
                            kv("requestId", extractedTextMessage.getRequestId()));

                    switch (extractedTextMessage.getFormat()) {
                        case TEI_XML:
                            xmlProcessor.handleMessage(extractedTextMessage, "teiHeader", corefPipeline);
                            break;
                        case ORLANDO_XML:
                            xmlProcessor.handleMessage(extractedTextMessage, "ORLANDOHEADER", corefPipeline);
                            break;
                        case MODS:
                        case EAD:
                        case PDF:
                        case HTML:
                        case PLAIN_TEXT:
                            plaintextProcessor.handleMessage(extractedTextMessage, defaultPipeline);
                        default:
                            // TODO
                    }
                })
        );
    }
}
