package ca.sharcnet.nerve.stanford.infrastructure.config;

import ca.sharcnet.nerve.stanford.StanfordNERModuleConstants;
import ca.sharcnet.nerve.stanford.service.CorefNERPipeline;
import ca.sharcnet.nerve.stanford.service.DefaultNERPipeline;
import ca.sharcnet.nerve.stanford.service.XMLProcessorUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import static ca.sharcnet.nerve.broker.infrastructure.config.RMQConfiguration.*;
import static ca.sharcnet.nerve.stanford.StanfordNERModuleConstants.QUEUE_NAME;

@Profile(StanfordNERModuleConstants.PROFILE_NAME)
@Configuration
public class StanfordNERConfiguration {

    @Bean
    public DefaultNERPipeline defaultPipeline(XMLProcessorUtils xmlUtils) {
        return new DefaultNERPipeline(xmlUtils);
    }

    @Bean
    public CorefNERPipeline corefPipeline(XMLProcessorUtils xmlUtils)
    {
        return new CorefNERPipeline(xmlUtils);
    }

    @Bean
    public Queue stanfordNERQueue() {
        return QueueBuilder.durable(QUEUE_NAME)
                .withArgument(DLQ_KEY, DLX_APP_MESSAGES)
                .build();
    }

    @Bean
    public Binding stanfordNERBinding(@Qualifier(APP_MESSAGES_EXCHANGE) DirectExchange exchange) {
        return BindingBuilder.bind(stanfordNERQueue()).to(exchange)
                .with(QUEUE_NAME);
    }

    @Bean
    public XMLProcessorUtils xmlProcessorUtils(ObjectMapper objectMapper) {
        return new XMLProcessorUtils(objectMapper);
    }
}