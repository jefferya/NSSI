package ca.sharcnet.nerve.stanford.service;

import ca.sharcnet.nerve.broker.message.NSSIServiceMessage;
import ca.sharcnet.nerve.broker.message.ProgressMessage;
import ca.sharcnet.nerve.broker.service.RMQUtilsService;
import ca.sharcnet.nerve.standoff.model.Annotation;
import ca.sharcnet.nerve.standoff.service.StandoffConverter;
import ca.sharcnet.nerve.standoff.service.StandoffDocument;
import ca.sharcnet.nerve.stanford.StanfordNERModuleConstants;
import edu.stanford.nlp.util.Interval;
import edu.stanford.nlp.util.IntervalTree;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;
import java.util.*;
import java.util.stream.Collectors;

import static net.logstash.logback.argument.StructuredArguments.kv;

@Profile(StanfordNERModuleConstants.PROFILE_NAME)
@Service
public class XMLProcessor {
    private final StandoffConverter standoffConverter;
    private final XMLProcessorUtils xmlProcessorUtils;
    private final RMQUtilsService rmqUtils;

    private final String ORGANIZATION_TAG_KEY = "ORGANIZATION_TAG";
    private final String LOCATION_TAG_KEY = "LOCATION_TAG";
    private final String PERSON_TAG_KEY = "PERSON_TAG";
    private final String TITLE_TAG_KEY = "TITLE_TAG";

    private final Logger LOGGER = LoggerFactory.getLogger(XMLProcessor.class);

    @Autowired
    public XMLProcessor(StandoffConverter standoffConverter,
                        XMLProcessorUtils xmlProcessorUtils,
                        RMQUtilsService rmqUtils)
    {
        this.standoffConverter = standoffConverter;
        this.xmlProcessorUtils = xmlProcessorUtils;
        this.rmqUtils = rmqUtils;
    }

    public void handleMessage(NSSIServiceMessage message,
                              String headerTagName,
                              NERPipeline classifier)
    {
        try {
            StandoffDocument standoffDocument = createStandoffDocument(message);

            Map<String, String> context = message.getContext();
            List<String> entityTags = List.of(
                    context.getOrDefault(ORGANIZATION_TAG_KEY, ""),
                    context.getOrDefault(LOCATION_TAG_KEY, ""),
                    context.getOrDefault(PERSON_TAG_KEY, ""),
                    context.getOrDefault(TITLE_TAG_KEY, "")
            ).stream().filter(Objects::nonNull).collect(Collectors.toList());

            int headerEnd = standoffDocument.findFirst(headerTagName)
                    .map(Annotation::getEnd)
                    .orElse(0);

            IntervalTree<Integer, Interval<Integer>> intervalsToIgnore = new IntervalTree<>();
            standoffDocument.getAnnotations().stream()
                    .filter(annotation -> entityTags.contains(annotation.getLabel()))
                    .filter(annotation -> annotation.getStart() > headerEnd)
                    .forEach(annotation ->
                            intervalsToIgnore.add(Interval.toInterval(annotation.getStart(), annotation.getEnd()))
                    );

            String text = standoffDocument.getPlain();
            List<Annotation> sections = standoffDocument.getAnnotations().stream()
                    .filter(annotation -> "p".equals(annotation.getLabel().toLowerCase(Locale.ROOT)))
                    .collect(Collectors.toList());

            LOGGER.info("CLASSIFIER: # of sections: " + sections.size());

            for (Annotation annotation : sections) {
                LOGGER.info("CLASSIFIER: Processing section from " + annotation.getStart() + " to " + annotation.getEnd());
                String annotationText = text.substring(annotation.getStart(), annotation.getEnd());
                var result = classifier.run(annotationText, annotation.getStart()).stream()
                        .map(r -> r.filterMatchesByEndPosition(headerEnd)
                                .flatMap(r2 -> r2.filterMatchesByOverlap(intervalsToIgnore))
                        )
                        .filter(Optional::isPresent)
                        .map(Optional::get)
                        .collect(Collectors.toList());

                sendNamedEntityMessages(message, result);
            }
        } catch (ParserConfigurationException | SAXException | IOException e) {
            LOGGER.error("FAILED To Process XML File",
                        kv("stackTrace", e.getStackTrace()),  
                        kv("service", StanfordNERModuleConstants.SERVICE_NAME),
                        kv("jobId", message.getJobId()),
                        kv("requestId", message.getRequestId()));
        }
    }

    private StandoffDocument createStandoffDocument(NSSIServiceMessage message)
            throws ParserConfigurationException, IOException, SAXException
    {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = dbf.newDocumentBuilder();
        Document teiDocument = builder.parse(new InputSource(new StringReader(message.getDocument())));
        return standoffConverter.toStandoff(teiDocument);
    }

    private void sendNamedEntityMessages(NSSIServiceMessage message,
                                         List<CorrelatedNERResults> classifierResults)
    {
        if (classifierResults.size() == 0) {
            LOGGER.info("No entities found.",
                    kv("service", StanfordNERModuleConstants.SERVICE_NAME),
                    kv("jobId", message.getJobId()),
                    kv("requestId", message.getRequestId()));

            ProgressMessage progressMessage = ProgressMessage.builder()
                    .jobId(message.getJobId())
                    .requestId(message.getRequestId())
                    .serviceName(StanfordNERService.class.getName())
                    .completed(true)
                    .build();

            rmqUtils.sendProgressMessage(progressMessage);
        } else {
            var namedEntities = xmlProcessorUtils.getNamedEntityInfo(classifierResults);
            var resultMessages = xmlProcessorUtils.convertNamedEntityResults(namedEntities, message);

            resultMessages.forEach(rmqUtils::sendToNextStep);

            LOGGER.info("Sending Progress Message",
                    kv("service", StanfordNERModuleConstants.SERVICE_NAME),
                    kv("jobId", message.getJobId()),
                    kv("size", classifierResults.size()),
                    kv("requestId", message.getRequestId()));

            ProgressMessage progressMessage = ProgressMessage.builder()
                    .jobId(message.getJobId())
                    .requestId(message.getRequestId())
                    .serviceName(StanfordNERService.class.getName())
                    .numStarted(classifierResults.size())
                    .build();

            rmqUtils.sendProgressMessage(progressMessage);
        }
    }
}
